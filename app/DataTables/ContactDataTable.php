<?php

namespace App\DataTables;

use App\Models\Contact as model;

use Illuminate\Database\Eloquent\Builder;

class ContactDataTable extends DataTable
{
    // 
    public $resourceName='contacts',$translation=[];
    public function __construct(model $model) 
    {
        $this->model = $model ;
        $this->togglableColumns=['is_open'];
        $this->actionsColumns = ['show','delete'];
        $this->translations= [];
		parent::__construct();
        $this->query= $this->query;
    }

    public function query(): Builder
    {
        $this->query= $this->model
        ->orderBy('id',$this->sortType??'desc')
        ->when(request()->trashOnly,function($q){
                return $q->onlyTrashed();
            })
            ->when(request()->keyword,function($q){
                return $q->where(function($q){
                    return $q->where(function($q){
                        
                    });
                });
            })
            ->when(request()->from_created_at || request()->to_created_at,function($q){
                return $q->where(function($q){
                    return $q->where(function($q){
                        $start= request()->from_created_at??'2000-00-00';
                        $end= request()->to_created_at??'3000-00-00';
                        return $q->whereBetween('created_at',[$start,$end]);
                    });
                });
            });
            

            if($this->sortBy == 'created_at'){
                $this->query= $this->query->orderBy($this->sortBy,$this->sortType);
            }
            
            return $this->query;
    }
    public function getColumns() :array
    {
        
        return [
            'id',
            'name',
            'phone',
            'created_at',
        ];
    }
    
}

