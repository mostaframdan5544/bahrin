<?php

namespace App\DataTables;

use App\Models\Category as model;
use Illuminate\Database\Eloquent\Builder;

class CategoryDataTable extends DataTable
{
    // 
    public $resourceName='categories',$translations='categories_translations';
    public function __construct(model $model) 
    {
        $this->model = $model ;
        $this->actionsColumns = ['create','edit','delete'];
        $this->translations= config('app.'.$this->translations);
		parent::__construct();
        $this->query= $this->query;
    }

    public function query(): Builder
    {
        $this->query= $this->model
        ->orderBy('id',$this->sortType??'desc')
        ->when(request()->trashOnly,function($q){
                return $q->onlyTrashed();
            })
            ->when(request()->keyword,function($q){
                return $q->where(function($q){
                    return $q->where(function($q){
                        return $q->whereHas('translations',function($q){
                                $q->where('content', 'like', "%" . request()->keyword . "%");
                        });
                    });
                });
            })
            ->when(request()->from_created_at || request()->to_created_at,function($q){
                return $q->where(function($q){
                    return $q->where(function($q){
                        $start= request()->from_created_at??'2000-00-00';
                        $end= request()->to_created_at??'3000-00-00';
                        return $q->whereBetween('created_at',[$start,$end]);
                    });
                });
            })
            ->when(request()->subCategories,function($q){
                return $q->whereNotNull('category_id');
            })
            ->when(!request()->subCategories,function($q){
                return $q->whereNull('category_id');
            });;

            if($this->sortBy == 'created_at'){
                $this->query= $this->query->orderBy($this->sortBy,$this->sortType);
            }
            elseif(in_array($this->sortBy,$this->translations)){
                $this->query= $this->query->with(['translations'=>function($q){
                    return $q->orderBy('content',$this->sortType);
                }]);
            }
            return $this->query;
    }
    public function getColumns() :array
    {
        
        return [
            'id',
            'content',
            'created_at',
        ];
    }
    
}

