<?php

namespace App\DataTables;

use App\Models\Notification as model;

class notificationDataTable extends DataTable
{
    // 
    public $resourceName='notifications',$translations='notifications_translations';
    public function __construct(model $model) 
    {
        $this->model = $model ;
        $this->actionsColumns = ['create','edit','delete'];
        $this->translations= config('app.'.$this->translations);
		parent::__construct();
        $this->query= $this->query
            ->when(request()->keyword,function($q){
                return $q->where(function($q){
                    return $q->where(function($q){
                        return $q->whereHas('translations',function($q){
                                $q->where('content', 'like', "%" . request()->keyword . "%");
                        });
                    });
                });
            })
            ->where(function($q){
                // 
            });
            if($this->sortBy == 'created_at'){
                $this->query= $this->query->orderBy($this->sortBy,$this->sortType);
            }
            elseif(in_array($this->sortBy,$this->translations)){
                $this->query= $this->query->with(['translations'=>function($q){
                    return $q->orderBy('content',$this->sortType);
                }]);
            }
    }
    public function getColumns() :array
    {
        
        return [
            'id',
            'content',
            'created_at',
        ];
    }
    
}

