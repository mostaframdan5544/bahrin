<?php

namespace App\DataTables;

use App\Models\Admin as model;

class AdminDataTable extends DataTable
{
    // 
    public $resourceName='admins',$translations='roles_translations';
    private
        $actionsColumns = ['create','edit','delete','show'];
    public function __construct(model $model) 
    {
        $this->model = $model ;
        $this->translations= config('app.'.$this->translations);
		parent::__construct();
        $this->query= $this->query
            ->where(function($q){
                // 
            });
            if($this->sortBy == 'created_at'){
                $this->query= $this->query->orderBy($this->sortBy,$this->sortType);
            }
    }
    public function getColumns() :array
    {
        
        return [
            'id',
            'name',
            'email',
            'created_at',
        ];
    }
    
}
