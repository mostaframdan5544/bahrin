<?php

namespace App\DataTables;

use Illuminate\Database\Eloquent\Builder;

class DataTable
{
    public $query,$model,$currentPage=1,
           $itemsPerPage=10,
           $sortBy,
           $sortType,
           $viewPath,
           $routeNamePrefix,
           $togglableColumns = [
               'is_active',
           ];
    private
        $actionsColumns = ['create','edit','delete','show'];
       

    public function __construct()
    {
        $this->itemsPerPage= request()->itemsPerPage??$this->itemsPerPage;
        $this->currentPage= ((int)request()->page )?  (int)request()->page : 1;
        $this->sortType= request()->sortType??'desc';
        $this->sortBy= request()->sortBy??'id';
        $this->viewPath= config('app.dashboardFolder').'pages.'.$this->resourceName.'.';
        $this->routeNamePrefix= $this->resourceName.'.';
        $this->query= $this->query();
    }

    /**
     * Build tableInfo .
     *
     * @param mixed $query Results from query() method.
     * @return string
     */


    public function main() : string
    {
        return (string) view($this->viewPath.'main',[
            'module'=>$this,
            'columns'=>$this->getColumns(),
            'routeNamePrefix'=>$this->routeNamePrefix,
        ]);
    }
    /**
     * Build tableInfo .
     *
     * @param mixed $query Results from query() method.
     * @return string
     */


    public function tableInfo() : string
    {
        return (string) view($this->viewPath.'tableinfo',[
            'records'=>$this->queryResult(),
            'columns'=>$this->getColumns(),
            'routeNamePrefix'=>$this->routeNamePrefix,
            'model'=>$this->model->getTable(),
            "togglableColumns"=>$this->togglableColumns,
            'module'=>$this,

            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */

    public function query(): Builder
    {
        $this->query= $this->model
        ->orderBy('id',$this->sortType??'desc')
        ->when(request()->trashOnly,function($q){
                return $q->onlyTrashed();
            })
            ->when(request()->from_created_at || request()->to_created_at,function($q){
                return $q->where(function($q){
                    return $q->where(function($q){
                        $start= request()->from_created_at??'2000-00-00';
                        $end= request()->to_created_at??'3000-00-00';
                        return $q->whereBetween('created_at',[$start,$end]);
                    });
                });
            });
            return $this->query;
    }
    public function queryResult()
    {
        return $this->query
                    ->forPage($this->currentPage,$this->itemsPerPage)
                    ->get();
    }
    /**
     * paigination html of this resource.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function paginationHtml() :string
    {
        
        $recordsCount= $this->query()->count();
        $totalPages= ceil($recordsCount/$this->itemsPerPage);
        return (string) view(config('app.dashboardFolder').'buttons.pagination',['totalPages'=>$totalPages,'currentPage'=>$this->currentPage]);

    }

    /**
     * Get columns.
     *
     * @return array
     */
    
}
