<?php

namespace App\DataTables;

use App\Models\Setting as model;
use Illuminate\Database\Eloquent\Builder;

class AppSettingDataTable extends DataTable
{
    // 
    public $resourceName='app_settings',$translations='settings_translations';
    public function __construct(model $model) 
    {
        $this->model = $model ;
        $this->actionsColumns = ['edit','delete'];
        $this->togglableColumns=[];

        $this->translations= config('app.'.$this->translations);
		parent::__construct();
        $this->query= $this->query;
    }

    public function query(): Builder
    {
        $this->query= $this->model
            ->orderBy('id',$this->sortType??'desc');
        return $this->query;
    }

    public function main() : string
    {
        return (string) view($this->viewPath.'main',[
            'module'=>$this,
            'columns'=>$this->getColumns(),
            'routeNamePrefix'=>$this->routeNamePrefix,
        ]);
    }
    public function getColumns() :array
    {
        
        return [
            'updated_at',
        ];
    }
    
}

