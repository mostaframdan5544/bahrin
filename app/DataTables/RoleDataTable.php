<?php

namespace App\DataTables;

use App\Models\Role as model;

class RoleDataTable extends DataTable
{
    // 
    public $resourceName='roles',$translations='roles_translations';
    public function __construct(model $model) 
    {
        $this->model = $model ;
        $this->translations= config('app.'.$this->translations);
		parent::__construct();
        $this->query= $this->query
            ->where(function($q){
                // 
            });
            if($this->sortBy == 'created_at'){
                $this->query= $this->query->orderBy($this->sortBy,$this->sortType);
            }
    }
    public function getColumns() :array
    {
        
        return [
            'id',
            'name',
            'created_at',
        ];
    }
    
}
