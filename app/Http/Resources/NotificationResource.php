<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->notification_id,
            'content'=>$this->Notification
                            ->translations
                            ->where('language_id',$request->language_id)
                            ->first()
                            ->content,
            "createdAt"=>strtotime($this->created_at),
            "is_seen"=>(bool)$this->is_seen
        ];
    }
}
