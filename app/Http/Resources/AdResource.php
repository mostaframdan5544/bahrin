<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data= [
            'id'=>$this->id,
            'link'=>$this->link??null,
        ];
        !$this->images??[]?:$data['image']= request()->root().$this->image->filename;
        !$this->store?:$data['store']= new StoreResource($this->store);
        return $data;
    }
}
