<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data=  [
            'id'=>$this->id,
            'name'=>$this->translations
                         ->where('language_id',$request->language_id)
                         ->where('type','name')
                         ->first()
                         ->content,
            "subCategoryId"=>$this->category_id

        ];
        !$this->images??[]?:$data['image']= request()->root().$this->image->filename;
        !$this->Branches->count()?:$data['branches']= BranchResource::collection($this->Branches);

        return $data;
    }
}
