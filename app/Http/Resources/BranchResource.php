<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data=[
            'id'=>$this->id,
            'name'=>$this->translations
                    ->where('language_id',$request->language_id)
                    ->first()
                    ->content,
            "address"=>$this->address??null,
            "whatsappLink"=>$this->whatsappLink??null,
            "phone"=>$this->phone??null,
            "instaLink"=>$this->instaLink,

        ] ;
        return $data;
    }
}
