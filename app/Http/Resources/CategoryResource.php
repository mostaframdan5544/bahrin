<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data=[
            'id'=>$this->id,
            'name'=>$this->translations
                    ->where('language_id',$request->language_id)
                    ->first()
                    ->content,

        ] ;
        !$this->images??[]?:$data['image']= request()->root().$this->image->filename;
        !$this->category_id?:$data['categoryId']= $this->category_id;
        return $data;
    }
}
