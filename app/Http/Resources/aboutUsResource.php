<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class aboutUsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->translations
            ->where('type','about')
            ->where('language_id',$request->language_id)
            ->first()
            ->content;
    }
}
