<?php
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Cookie;
use Config;

class GeneralSettingController 
{
    static function changeLang($lang)
    {
        
        return back();
    }
    function changeStatus($model, $col_name, $id  )
    {
        $model=  Str::singular(Str::studly($model));

        $record="App\Models\\{$model}"::find($id);
        $record->update([$col_name=>$record->$col_name?0:1]);
    }

    
    function deleteRecord(Request $request)
    {
        $delteFunc= Str::contains(url()->previous(),'trashOnly=true')?'forceDelete':'delete';
        $model=  Str::singular(Str::studly($request->model));
        $model= "App\Models\\".str_replace('-','',$model) ;
        $ids=[];
        if($request->has('ids'))
            $ids=explode(',', $request->ids??[]);
        if($request->has('id'))
            $ids[]=$request->id;

        $model::whereIn('id',$ids)->$delteFunc();
        return response()->json(['status'=>200]);
    }

    function restoreRecord(Request $request)
    {
        $model=  Str::singular(Str::studly($request->model));
        $model= "App\Models\\".str_replace('-','',$model) ;
        $ids=[];
        if($request->has('ids'))
            $ids=explode(',', $request->ids??[]);
        if($request->has('id'))
            $ids[]=$request->id;

        $model::whereIn('id',$ids)->restore();
        return response()->json(['status'=>200]);
    }
}
