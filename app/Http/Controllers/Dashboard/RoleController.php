<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Requests\Dashboard\StoreRoleRequest as StoreRequest;
use App\Http\Requests\Dashboard\UpdateRoleRequest as UpdateRequest;
use App\Models\Role as model;
use Illuminate\Http\Request;
use App\DataTables\RoleDataTable as DataTable;
use App\Roles\RoleController as Role;
use Illuminate\Support\Str;

class RoleController extends DataTable
{

    /**
     * Display a listing of the resource.
     * @return
     */
    public function index(DataTable $module)
    {
        return request()->ajax() ?
            $module->main() :
            view($this->viewPath . 'index', compact('module'));
    }

    public function create(DataTable $module)
    {
        return view($this->viewPath . 'create',compact('module'));
    }

    public function store( StoreRequest $request)
    {
       
        model::create([
            'name'=>$request->name,
            'permissions'=>$request->permissions,
            'createBy_id'=>AuthLogged()->id,
            'createBy_type'=>AuthLogged()->modelPath,
            'created_at' => now(),

        ]);
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id, DataTable $module)
    {

        $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'edit', compact('record','module'));
    
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateRequest $request,  $id)
    {
        $record= model::withTrashed()->find($id);
        $record->update([
            'name'=>$request->name,
            'permissions'=>$request->permissions,
            'updated_at'=>now(),
            'updateBy_id'=>AuthLogged()->id,
            'updateBy_type'=>AuthLogged()->modelPath,
        ]);
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');

    }
}