<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Requests\Dashboard\UpdateSettingRequest as UpdateRequest;
use App\Models\Setting as model;
use App\Models\SettingContent as modelTranslation;
use App\Models\Image;
use App\Models\Store;
use Illuminate\Http\Request;
use App\DataTables\AppSettingDataTable as DataTable;
use Illuminate\Support\Str;

class AppSettingController extends DataTable
{

    /**
     * Display a listing of the resource.
     * @return
     */
    public function index(DataTable $module)
    {
        return request()->ajax() ?
            $module->main() :
            view($this->viewPath . 'index', compact('module'));
    }

    public function create(DataTable $module)
    {
        return view($this->viewPath . 'create',compact('module'));
    }

    public function store( StoreRequest $request)
    {
        $Alltargets=[];
        $data=[];
        $record = model::create([
            'phone'=>$request->phone,
            'address'=>$request->address,
            'whatsappLink'=>$request->whatsappLink,
            'instaLink'=>$request->instaLink,
            'store_id'=>$request->store_id,
            'createBy_id'=>AuthLogged()->id,
            'createBy_type'=>AuthLogged()->modelPath,
            'created_at' => now(),
        ]);
        foreach ($this->translations as $translation){
            foreach (languages() as $lang){
                modelTranslation::updateOrCreate(
                    [Str::singular($record->getTable()).'_id'=>$record->id,'language_id'=>$lang->id,'type'=>$translation],
                    [
                        'content'=>$request->input($translation.'_'.$lang->name),
                        'type'=>$translation
                    ]
                );
            }
        }
        if($request->has('image'))
            Image::updateOrCreate([
                'imageable_type'=>'App\Models\Category',
                'imageable_id'=>$record->id,
                'type'=>'main',
            ],[
                'filename'=>uploadPhoto($request->image,'Category'),
                
                'createBy_id'=>AuthLogged()->id,
                'createBy_type'=>AuthLogged()->modelPath,
                'created_at' => now(),
            ]);
       
        
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');
    }

    /**
     * Show the specified resource.
     * @param school $vendor
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */

    public function show($id)
    {
       $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'show', compact('record'));

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id, DataTable $module)
    {

        $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'edit', compact('record','module'));
    
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateRequest $request,  $id)
    {
        $record= model::find($id);
        $record->update([
            'email'=>$request->email,
            'phone'=>$request->phone,
            'updated_at'=>now(),
            'updateBy_id'=>AuthLogged()->id,
            'updateBy_type'=>AuthLogged()->modelPath,
        ]);
        foreach ($this->translations as $translation){
            foreach (languages() as $lang){
                modelTranslation::updateOrCreate(
                    [Str::singular($record->getTable()).'_id'=>$record->id,'language_id'=>$lang->id,'type'=>$translation],
                    [
                        'content'=>$request->input($translation.'_'.$lang->name),
                        'type'=>$translation
                    ]
                );
            }
        }
        if($request->has('image')){

            deleteFile($record->image->filename);
            Image::updateOrCreate([
                'imageable_type'=>'App\Models\Setting',
                'imageable_id'=>$record->id,
                'type'=>'main',
            ],[
                'filename'=>uploadPhoto($request->image,'Setting'),
                
                'createBy_id'=>AuthLogged()->id,
                'createBy_type'=>AuthLogged()->modelPath,
                'created_at' => now(),
            ]);
        }
       
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');

    }
}