<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Requests\Dashboard\StoreAdminRequest ;
use App\Http\Requests\Dashboard\UpdateAdminRequest as UpdateRequest;
use App\Models\Admin as model;
use Illuminate\Http\Request;
use App\DataTables\AdminDataTable as DataTable;
use App\Models\Role;
use Illuminate\Support\Str;

class AdminController extends DataTable
{

    /**
     * Display a listing of the resource.
     * @return
     */
    public function index(DataTable $module)
    {
        return request()->ajax() ?
            $module->main() :
            view($this->viewPath . 'index', compact('module'));
    }

    public function create(DataTable $module)
    {
        return view($this->viewPath . 'create',compact('module'),['roles'=>Role::all()]);
    }


    /**
     * Show the specified resource.
     * @param school $vendor
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */

    public function show($id)
    {
       $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'show', compact('record'));

    }
    public function store( StoreAdminRequest $request)
    {
        model::create([
            'name'=>$request->name,
            'createBy_id'=>AuthLogged()->id,
            'createBy_type'=>AuthLogged()->modelPath,
            'created_at' => now(),
            'email'=>$request->email,
            'phone'=>$request->phone,
            'role_id'=>$request->role_id,
            'password'=>bcrypt($request->password),

        ]);
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit( DataTable $module, $id)
    {
        $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'edit', compact('record','module'),['roles'=>Role::all()]);
    
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateRequest $request,  $id)
    {
        $record= model::withTrashed()->find($id);
        $record->update([
            'email'=>$request->email,
            'phone'=>$request->phone,
            'password'=>bcrypt($request->password),
            'role_id'=>$request->role_id,
            'updated_at'=>now(),
            'updateBy_id'=>AuthLogged()->id,
            'updateBy_type'=>AuthLogged()->modelPath,
        ]);
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');

    }
}