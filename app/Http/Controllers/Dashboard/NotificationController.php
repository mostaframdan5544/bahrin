<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Requests\Dashboard\StoreNotificationRequest as StoreRequest;
use App\Http\Requests\Dashboard\UpdateNotificationRequest as UpdateRequest;
use App\Models\Notification as model;
use App\Models\NotificationContent as modelTranslation;
use App\Models\User;
use Illuminate\Http\Request;
use App\DataTables\notificationDataTable as DataTable;
use App\Notifications\notificationController as Notification;
use Illuminate\Support\Str;

class NotificationController extends DataTable
{

    /**
     * Display a listing of the resource.
     * @return
     */
    public function index(DataTable $module)
    {
        return request()->ajax() ?
            $module->main() :
            view($this->viewPath . 'index', compact('module'));
    }

    public function create(DataTable $module)
    {
        return view($this->viewPath . 'create',compact('module'));
    }

    public function store( StoreRequest $request)
    {
        $Alltargets=[];
        $data=[];
        foreach ($this->translations as $translation){
            foreach (languages() as $lang){
                $data[$translation][$lang->name]=$request->input($translation.'_'.$lang->name);
            }
        }
        
        new Notification($Alltargets,$data,[
            'createBy_id'=>AuthLogged()->id,
            'createBy_type'=>AuthLogged()->modelPath,
            'created_at'=>now()
        ]);
        
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');
    }

    /**
     * Show the specified resource.
     * @param school $vendor
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */

    public function show($id)
    {
       $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'show', compact('record'));

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id, DataTable $module)
    {

        $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'edit', compact('record','module'));
    
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateRequest $request,  $id)
    {
        $record= model::find($id);
        $record->update([
            'updated_at'=>now(),
            'updateBy_id'=>AuthLogged()->id,
            'updateBy_type'=>AuthLogged()->modelPath,
        ]);
        foreach ($this->translations as $translation){
            foreach (languages() as $lang){
                modelTranslation::updateOrCreate(
                    [Str::singular($record->getTable()).'_id'=>$id,'language_id'=>$lang->id,'type'=>$translation],
                    [$translation=>$request->input($translation.'_'.$lang->name)]
                );
            }
        }
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');

    }
}