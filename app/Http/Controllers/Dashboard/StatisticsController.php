<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\User;
use App\Models\Store;
use App\Models\Category;
use App\Models\admins;
use DB;

class StatisticsController 
{

    /**
     * Display a listing of the resource.
     * @return
     */
    public function index()
    {
        $total_users = User::count();
        $total_stores = Store::count();
        $total_categories = Category::count('id');
        return  view(config('app.dashboardFolder').'pages.statistics.index',compact('total_users','total_stores','total_categories'));
    }
}