<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\CreateUpdateClassRequest;
use Illuminate\Http\Request;
use App\DataTables\ContactDataTable;
use App\Models\Contact as model;

class ContactController extends ContactDataTable
{

    /**
     * Display a listing of the resource.
     * @return
     */
    public function index(ContactDataTable $module)
    {
        return  request()->ajax() ?
                $module->main():
                view($this->viewPath.'index', compact('module')) ;
    }
    

    public function show($id)
    {
        $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'show', compact('record'));

    }

}

