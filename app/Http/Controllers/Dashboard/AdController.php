<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Requests\Dashboard\StoreAdRequest as StoreRequest;
use App\Http\Requests\Dashboard\UpdateAdRequest as UpdateRequest;
use App\Models\Ad as model;
use App\Models\AdContent as modelTranslation;
use App\Models\Image;
use App\Models\Store;
use Illuminate\Http\Request;
use App\DataTables\AdDataTable as DataTable;
use Illuminate\Support\Str;

class AdController extends DataTable
{

    /**
     * Display a listing of the resource.
     * @return
     */
    public function index(DataTable $module)
    {
        return request()->ajax() ?
            $module->main() :
            view($this->viewPath . 'index', compact('module'));
    }

    public function create(DataTable $module)
    {
        return view($this->viewPath . 'create',compact('module'),[
            'stores'=>Store::all()
        ]);
    }

    public function store( StoreRequest $request)
    {
        $Alltargets=[];
        $data=[];
        $record = model::create([
            'store_id'=>$request->store_id,
            'link'=>$request->link,
            'start_at'=>$request->start_at,
            'end_at'=>$request->end_at,
            'createBy_id'=>AuthLogged()->id,
            'createBy_type'=>AuthLogged()->modelPath,
            'created_at' => now(),
        ]);
        
        Image::updateOrCreate([
            'imageable_type'=>'App\Models\Ad',
            'imageable_id'=>$record->id,
            'type'=>'main',
        ],[
            'filename'=>uploadPhoto($request->image,'Ad'),
            
            'createBy_id'=>AuthLogged()->id,
            'createBy_type'=>AuthLogged()->modelPath,
            'created_at' => now(),
        ]);
       
        
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');
    }

    /**
     * Show the specified resource.
     * @param school $vendor
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */

    public function show($id)
    {
       $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'show', compact('record'));

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id, DataTable $module)
    {

        $record= model::withTrashed()->find($id);
        return view($this->viewPath . 'edit', compact('record','module'),[
            'stores'=>Store::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(UpdateRequest $request,  $id)
    {
        $record= model::find($id);
        $record->update([
            'store_id'=>$request->store_id,
            'link'=>$request->link,
            'start_at'=>$request->start_at,
            'end_at'=>$request->end_at,
            'updated_at'=>now(),
            'updateBy_id'=>AuthLogged()->id,
            'updateBy_type'=>AuthLogged()->modelPath,
        ]);
       
        if($request->has('image')){

            if($record->image)
                deleteFile($record->image->filename);
            Image::updateOrCreate([
                'imageable_type'=>'App\Models\Ad',
                'imageable_id'=>$record->id,
                'type'=>'main',
            ],[
                'filename'=>uploadPhoto($request->image,'Ad'),
                
                'createBy_id'=>AuthLogged()->id,
                'createBy_type'=>AuthLogged()->modelPath,
                'created_at' => now(),
            ]);
        }
       
        session()->flash('success', __('the_process_completed_successful'));
        return redirect()->route($this->routeNamePrefix . 'index');

    }
}