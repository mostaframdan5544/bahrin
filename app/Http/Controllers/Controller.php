<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use \Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function sendResponse($result,int $status, string $message='',$dataName,$extre=[])
    {
        $response = [
            'success' => true,
            'status'  => $status,
            'message' => $message,
            $dataName   => $result,
            // $dataName   => $result->response()->getData(),
        ];
        foreach($extre as $k=>$v){
            $response[$k]= $v; 
        }
        return response()->json($response, $status);

    }
}
