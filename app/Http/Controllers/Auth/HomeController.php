<?php 

namespace App\Http\Controllers\Auth;

class HomeController
{
    public  $adminRoute,$schoolRoute;
    public function __construct()
    {
        $this->adminRoute=route('dashboard.welcome');
    }

    public function redirectAfterLogin()
    {
        $authLogged= AuthLogged();
        if($authLogged){
            return redirect($this->adminRoute);
        }else{
            return redirect(route('dashboard.login.index'));
        }
    }
}