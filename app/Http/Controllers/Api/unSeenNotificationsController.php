<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\unSeenNotificationsRequest;
use App\Models\Notify;

class unSeenNotificationsController extends Controller
{
    public function index(unSeenNotificationsRequest $request)
    {
        $count   = Notify::where('user_id',$request->user->id)
                        ->where('is_seen',0)
                        ->count();
        return response()->json(['status'=>200,'count'=>$count]);
    }

}
