<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\storeDetailsRequest;
use App\Http\Resources\StoreResource;
use App\Models\Store;

class storeDetailsController extends Controller
{
    public function index(storeDetailsRequest $request)
    {
        $record = Store::checkContent($request,'')
                    ->whereId($request->storeId)
                    ->first();

            $data=  new StoreResource($record);

            return $this->sendResponse($data, 200, '','storeDetails');
    }

}
