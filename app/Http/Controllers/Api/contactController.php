<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\contactRequest;
use App\Models\Contact;

class contactController extends Controller
{
    public function index(contactRequest $request)
    {
        Contact::create([
            'name'=>$request->name,
            'phone'=>$request->phone,
            'message'=>$request->message,
            'created_at'=>now(),
        ]);
        return response()->json(['status'=>200]);
    }

}
