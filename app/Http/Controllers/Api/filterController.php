<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\filterRequest ;
use App\Models\Store;
use App\Http\Resources\StoreResource;

class filterController extends Controller
{
    public function index(filterRequest $request)
    {
        $records = Store::checkContent($request,'')
        ->when($request->keyword,function($q)use($request){
            return $q->whereHas('translations',function($q)use ($request){
                return $q->where('content','Like',"%{$request->keyword}%");
            });
        })
        ->when($request->subCategoryId,function($q)use($request){
                return $q->where('category_id',$request->subCategoryId);
        })
        ->when($request->districtId,function($q)use($request){
            return $q->where('district_id',$request->districtId);
        });

        $data=  StoreResource::collection($records->paginate(config('app.itemsPerPage')));

        return $this->sendResponse($data, 200, '','stores',["resultCount"=>$records->count()]);
    
    }

}
