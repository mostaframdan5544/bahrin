<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\getDistrictsRequest;
use App\Http\Resources\DistrictResource;
use App\Models\District;

class getDistrictsController extends Controller
{
    public function index(getDistrictsRequest $request)
    {
        $records = District::where('is_active',1)
                        ->checkContent($request,'')
                        ->paginate(config('app.itemsPerPage'));

        $data=  DistrictResource::collection($records);
        
        return $this->sendResponse($data, 200, '','districts');
    }


}
