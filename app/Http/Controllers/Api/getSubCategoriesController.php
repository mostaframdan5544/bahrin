<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\getSubCategoriesRequest;

use App\Http\Resources\CategoryResource;

use App\Models\Category;

class getSubCategoriesController extends Controller
{
    public function index(getSubCategoriesRequest $request)
    {
        $records = Category::checkContent($request,'')
                        ->where('is_active',1)
                        ->where('category_id',$request->categoryId)
                        ->paginate(config('app.itemsPerPage'));

        $data=  CategoryResource::collection($records);
        
        return $this->sendResponse($data, 200, '','subCategories');
    }

}
