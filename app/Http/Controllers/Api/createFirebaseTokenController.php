<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\createFirebaseTokenRequest;
use App\Models\User;

class createFirebaseTokenController extends Controller
{
    public function index(createFirebaseTokenRequest $request)
    {
        User::updateOrCreate([
            'device_id'=> $request->deviceId,
            'firebase_token'=> $request->firebaseToken,
        ]);
        return response()->json(['status'=>200]);
    }
}
