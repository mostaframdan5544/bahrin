<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\aboutUsRequest;
use App\Http\Resources\aboutUsResource;
use App\Models\Setting;

class aboutUsController extends Controller
{
    public function index(aboutUsRequest $request)
    {
        $record= Setting::whereHas('translations',function($q) use ($request){
                        return $q->where('language_id',$request->language_id)
                        ->where('type','about');
                    })
                    ->first();
        if($record){
            $data = $record->translations
                    ->where('type','about')
                    ->where('language_id',$request->language_id)
                    ->first()
                    ->content;
            return $this->sendResponse($data, 200, '','aboutUs');
        }

        return $this->sendResponse('', 204, '','aboutUs');

    }
}
