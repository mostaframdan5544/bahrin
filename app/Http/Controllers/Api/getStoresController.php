<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\getStoresRequest;
use App\Models\Store;
use App\Http\Resources\StoreResource;

class getStoresController extends Controller
{
    public function index(getStoresRequest $request)
    {
        $records = Store::checkContent($request,'')
                        ->where('is_active',1)
                        ->where('category_id',$request->subCategoryId)
                        ->paginate(config('app.itemsPerPage'));

        $data=  StoreResource::collection($records);
        
        return $this->sendResponse($data, 200, '','stores');
    }
}
