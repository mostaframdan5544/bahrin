<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\adsRequest;
use App\Http\Resources\AdResource;
use App\Models\Ad;

class adsController extends Controller
{
    public function index(adsRequest $request)
    {
        $records = Ad::where('is_active',1)
                    ->where('start_at','<=',date("Y-m-d H:i:s"))
                    ->where('end_at','>=',date("Y-m-d H:i:s"))
                    ->get();
        $data=  AdResource::collection($records);
        return $this->sendResponse($data, 200, '','ads');
    }

}
