<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\notificationsRequest;
use App\Models\Notification;
use App\Models\Notify;
use App\Http\Resources\NotificationResource;

class notificationsController extends Controller
{
    public function index(notificationsRequest $request)
    {
        $notifications = Notify::whereHas('Notification',function($q) use($request){
                                    return $q->checkContent($request);
                                })
                                ->paginate(config('app.itemsPerPage'));
        $data=  NotificationResource::collection($notifications);
        
        Notify::whereIn('id',$notifications->pluck('id'))
              ->update(['is_seen'=>1]);

        return $this->sendResponse($data, 200, '','notifications');
    }
}
