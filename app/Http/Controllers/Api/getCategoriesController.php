<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\getCategoriesRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;

class getCategoriesController extends Controller
{
    public function index(getCategoriesRequest $request)
    {
        $records = Category::where('is_active',1)
                        ->checkContent($request,'')
                        ->whereNull('category_id')
                        ->paginate(config('app.itemsPerPage'));

        $data=  CategoryResource::collection($records);
        
        return $this->sendResponse($data, 200, '','categories');
    }

}
