<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\searchRequest ;
use App\Models\Store;
use App\Http\Resources\StoreResource;

class searchController extends Controller
{
    public function index(searchRequest $request)
    {
        $records = Store::checkContent($request,'')
        ->when($request->keyword,function($q)use($request){
            return $q->whereHas('translations',function($q)use ($request){
                return $q->where('content','Like',"%{$request->keyword}%");
            });
        });

        $data=  StoreResource::collection($records->paginate(config('app.itemsPerPage')));

        return $this->sendResponse($data, 200, '','stores',["resultCount"=>$records->count()]);
    
    }

}
