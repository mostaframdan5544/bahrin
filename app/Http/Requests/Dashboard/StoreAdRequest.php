<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'store_id'=>'required_if:link,|exists:stores,id',
            'link'=>'required_if:store_id,',
            'start_at' =>'required|after: 0.0000005 seconds',
            'end_at' =>'required|after:start_at',
            "image" =>"image|mimes:jpeg,png,bmp,tiff|max:4096"
        ];
    }
}
