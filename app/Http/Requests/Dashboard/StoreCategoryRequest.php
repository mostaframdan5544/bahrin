<?php

namespace App\Http\Requests\Dashboard;


use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $data=[];
        foreach (languages() as $lang){
            foreach (config('app.categories_translations') as $translation){
                $data[$translation.'_'.$lang->name]='required|string';
            }
        }
        $data['image']= 'required|image|mimes:jpeg,png,bmp,tiff |max:4096';
        
        return $data;
    }
}
