<?php

namespace App\Http\Requests\Dashboard;

use Illuminate\Foundation\Http\FormRequest;

class StoreBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $data=[];
        foreach (languages() as $lang){
            foreach (config('app.branches_translations') as $translation){
                $data[$translation.'_'.$lang->name]='required|string';
            }
        }
        // $data['store_id']='required|exists:stores,id';
        // $data['address']='required';
        $data['phone']='required|unique:branches,phone,'.$this->id;
        $data['whatsappLink']='required';
        $data['instaLink']='required';
        return $data;
    }
}
