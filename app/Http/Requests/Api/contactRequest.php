<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class contactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
  
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            "deviceId"=>"nullable|exists:users,device_id",
            "language"=>"required|exists:languages,name",
            "name"=>"nullable",
            "phone"=>"nullable",
            "message"=>"required",
        ];
    }
}
