<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use  App\Traits\Models\scopeCheckContent;
use  App\Traits\Models\update_by;
use  App\Traits\Models\create_by;
use  App\Traits\Models\getContent;
use  App\Traits\Models\content;
use Illuminate\Database\Eloquent\Casts\Attribute;


class Store extends Model
{
    use HasFactory,SoftDeletes,
    create_by,update_by,scopeCheckContent, getContent,content;
    
    public $timestamps=false,$guarded=[],$table='stores';
    
    protected function categoryName(): Attribute
    {
        return Attribute::make(
            get: function () {
                if($this->category ){
                    return $this->category->content;
                }
            
            },
        );
    }
    function translations()
    {
        return $this->hasMany(StoreContent::class);
    }
    
    public function image(){
        return $this->morphOne(Image::class,'imageable');
    }

    function Branches()
    {
        return $this->hasMany(Branch::class);
    }

    public function category(){

        return $this->belongsTo(Category::class);

    }
}
