<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchContent extends Model
{
    use HasFactory;
    public $table='branches_content',$timestamps=false,$guarded=[];

}
