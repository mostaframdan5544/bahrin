<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use  App\Traits\Models\scopeCheckContent;
use  App\Traits\Models\update_by;
use  App\Traits\Models\create_by;
use  App\Traits\Models\getContent;
use  App\Traits\Models\content;

class Branch extends Model
{
    use HasFactory,SoftDeletes,
    create_by,update_by,scopeCheckContent, getContent,content;
    public $timestamps=false,$guarded=[],$table='branches';

    
    protected function storeName(): Attribute
    {
        return Attribute::make(
            get: function () {
                if($this->store ){
                    return $this->store->content;
                }
            
            },
        );
    }
    
    function store()
    {
        return $this->belongsTo(Store::class);
    }
    public function image(){
        return $this->morphOne(Image::class,'imageable');
    }
    function translations()
    {
        return $this->hasMany(BranchContent::class);
    }
}
