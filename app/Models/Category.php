<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use  App\Traits\Models\scopeCheckContent;
use  App\Traits\Models\update_by;
use  App\Traits\Models\create_by;
use  App\Traits\Models\getContent;
use  App\Traits\Models\content;

class Category extends Model
{
    use HasFactory,SoftDeletes,
    create_by,update_by,scopeCheckContent, getContent,content;
    public $timestamps=false,$guarded=[],$table='categories';    
    
    function translations()
    {
        return $this->hasMany(CategoryContent::class);
    }

    public function image(){
        return $this->morphOne(Image::class,'imageable');
    }

}
