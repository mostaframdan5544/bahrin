<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
    use HasFactory,SoftDeletes,\App\Traits\Models\create_by;
    public $timestamps=false,$guarded=[],$table='admins';

    protected function modelPath(): Attribute
    {
        return Attribute::make(
            get: fn () => '\App\Models\Admin',
        );
    }

    function role()
    {
        return $this->belongsTo(Role::class);
    }
}
