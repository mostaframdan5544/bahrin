<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Ad extends Model
{
    use HasFactory,SoftDeletes,\App\Traits\Models\create_by;
    public $timestamps=false,$guarded=[],$table='ads';

    protected function storeName(): Attribute
    {
        return Attribute::make(
            get: function () {
                if($this->store ){
                    return $this->store->content;
                }
            
            },
        );
    }

    public function image(){
        return $this->morphOne(Image::class,'imageable');
    }

    public function store(){
        return $this->belongsTo(Store::class);
    }
}
