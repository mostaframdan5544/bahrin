<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationContent extends Model
{
    use HasFactory;
    public $timestamps=false,$guarded=[],$table='notifactions_content';
    public function scopeCheckContent($query, $request,$type=null)
    {
        return $query->where('language_id',$request->language_id)
                ->when($type,function($q) use($type){
                    $q->whereType($type);
                });
    }
}