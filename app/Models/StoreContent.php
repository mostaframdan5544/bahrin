<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreContent extends Model
{
    use HasFactory;
    public $table='stores_content',$timestamps=false,$guarded=[];


}
