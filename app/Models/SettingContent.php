<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingContent extends Model
{
    use HasFactory;
    public $timestamps=false,$guarded=[],$table='settings_content';

}
