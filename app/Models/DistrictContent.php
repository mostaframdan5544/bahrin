<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DistrictContent extends Model
{
    use HasFactory;
    public $table='districts_content',$timestamps=false,$guarded=[];

}
