<?php


namespace App\Traits\Models;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait getContent{
 
    public function getContent($lang,$type=null) : string
    {
        return $this->translations
                ->where('language_id',$lang->id)
                ->when($type,function($q) use($type){
                    return $q->where('type',$type);
                })
                ->first()
                ->content??'';
    }

}