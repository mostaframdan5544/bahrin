<?php


namespace App\Traits\Models;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait scopeCheckContent{
 
    public function scopeCheckContent($query, $request,$type=null)
    {
        return $query->whereHas('translations',function($q) use ($type,$request){
            $q->where('language_id',$request->language_id)
                ->when($type,function($q) use($type){
                    $q->whereType($type);
                });
        } );
    }

}