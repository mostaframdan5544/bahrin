<?php


namespace App\Traits\Models;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait create_by{
 
    protected function createByAuth(): Attribute
    {
        return Attribute::make(
            get: function () {
                $model= $this->createBy_type;
                $id=  $this->createBy_id;
                if($model && $id){
                    return $model::find($id);
                }
            },
        );
    }

}