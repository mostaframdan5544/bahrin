<?php


namespace App\Traits\Models;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait content{
 
    protected function content(): Attribute
    {
        return Attribute::make(
            get: function () {
                if($this->translations->where('language_id',currentLang()->id)->count() > 0){
                    return $this->translations->where('language_id',currentLang()->id)->first()->content;
                }
            
            },
        );
    }


}