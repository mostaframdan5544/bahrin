<?php


namespace App\Traits\Models;
use Illuminate\Database\Eloquent\Casts\Attribute;

trait update_by{
 
    protected function updateByAuth(): Attribute
    {
        return Attribute::make(
            get: function () {
                $model= $this->updateBy_type;
                $id=  $this->updateBy_id;
                if($model && $id){
                    return $model::find($id);
                }
            },
        );
    }

}