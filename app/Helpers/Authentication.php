<?php

	use Illuminate\Support\Facades\Auth;
	use App\Models\Admin;

	//singloTon desgin pattern
	class Authentication{
		private static $Auth;
		private function __construct()
		{
		}

		public static function getAuth()
		{
			if(!self::$Auth)
			{
				if(Auth::guard('admin')->check())
					self::$Auth= Admin::find(Auth::guard('admin')->user()->id);
				else 
					self::$Auth= NULL;
			}
			return self::$Auth;
		}

		public static function setAuth($auth)
		{
			if(!self::$Auth)
				self::$Auth= $auth;
		}

	}