<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Middleware\setUSerByDeviceId;

Route::get('getFirstUser', function () {
    return \App\Models\User::find(2);
});



// Route::get('aboutUs',['App\Http\Controllers\Api\createFirebaseTokenController','index']);

Route::middleware([setUSerByDeviceId::class])->group(function () {
    Route::get('ads',['App\Http\Controllers\Api\adsController','index']);
    Route::get('aboutUs',['App\Http\Controllers\Api\aboutUsController','index']);
    Route::post('createFirebaseToken',['App\Http\Controllers\Api\createFirebaseTokenController','index']);
    Route::get('unSeenNotifications',['App\Http\Controllers\Api\unSeenNotificationsController','index']);
    Route::get('notifications',['App\Http\Controllers\Api\notificationsController','index']);
    Route::get('getCategories',['App\Http\Controllers\Api\getCategoriesController','index']);
    Route::get('getSubCategories',['App\Http\Controllers\Api\getSubCategoriesController','index']);
    Route::get('getStores',['App\Http\Controllers\Api\getStoresController','index']);
    Route::get('storeDetails',['App\Http\Controllers\Api\storeDetailsController','index']);
    Route::post('contact',['App\Http\Controllers\Api\contactController','index']);
    Route::get('search',['App\Http\Controllers\Api\searchController','index']);
    Route::get('getDistricts',['App\Http\Controllers\Api\getDistrictsController','index']);
    Route::get('filter',['App\Http\Controllers\Api\filterController','index']);

});
