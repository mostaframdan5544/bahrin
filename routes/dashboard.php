<?php 
use Illuminate\Support\Facades\Route;

use App\Http\Middleware\dashboardAuth;
use App\Http\Middleware\dashboardLang;



Route::group(['middleware' => [dashboardLang::class],"prefix"=>'/dashboard'], function () {
    
    Route::group(['middleware' => [dashboardAuth::class,dashboardLang::class]], function () {

        route::get('/home',"Auth\HomeController@redirectAfterLogin")->name('home');
        route::view('/welcome','dashboard.welcome')->name('dashboard.welcome');

        Route::get('changeStatus/{model}/{col_name}/{id}',['App\Http\Controllers\Dashboard\GeneralSettingController','changeStatus'])->name('dashboard.changeStatus');
        Route::get('/deleteRecord', ['App\Http\Controllers\Dashboard\GeneralSettingController','deleteRecord'])->name('dashboard.deleteRecord');
        Route::get('/restoreRecord', ['App\Http\Controllers\Dashboard\GeneralSettingController','restoreRecord'])->name('dashboard.restoreRecord');
        Route::get('/changeLnag/{lang}', ['App\Http\Controllers\Dashboard\GeneralSettingController','changeLang'])->name('dashboard.changeLnag');



        Route::resources([
            'notifications'=>'Dashboard\NotificationController',
            'roles'=>'Dashboard\RoleController',
            'admins'=>'Dashboard\AdminController',
            'app_settings'=>'Dashboard\AppSettingController',
            'categories'=>'Dashboard\CategoryController',
            'stores'=>'Dashboard\StoreController',
            'branches'=>'Dashboard\BranchController',
            'districts'=>'Dashboard\DistrictController',
            'ads'=>'Dashboard\AdController',
            'contacts'=>'Dashboard\ContactController',
        ]);

        Route::get('sortCategory',["App\Http\Controllers\Dashboard\CategoryController","sortCategory"])->name('sortCategory.index');

        Route::get('ResetPassword',["App\Http\Controllers\Auth\ResetPasswordController","index"])->name('resetPassword.index');
        Route::put('ResetPassword',["App\Http\Controllers\Auth\ResetPasswordController","updatePassword"])->name('resetPassword');

        Route::get('/statistics',"Dashboard\StatisticsController@index")->name('dashboard.statistics');
        Route::get('/',['App\Http\Controllers\Auth\HomeController','redirectAfterLogin'] )->name('home');


    

        Route::post('/logout', ['App\Http\Controllers\Auth\LoginController','logout'])->name('dashboard.logout');

        Route::post('/logout', ['App\Http\Controllers\Auth\LoginController','logout'])->name('dashboard.logout');

    });

    Route::get('login',["App\Http\Controllers\Auth\LoginController","index"])->name('dashboard.login.index');
    Route::post('login', ['App\Http\Controllers\Auth\LoginController','authenticate'])->name('dashboard.login');

});

 