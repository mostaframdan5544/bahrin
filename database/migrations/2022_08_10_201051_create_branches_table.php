<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->id();
            $table->softDeletes('deleted_at', 0);

            $table->morphs('createBy');
            $table->nullableMorphs('updateBy');

            $table->unsignedBigInteger('store_id')->nullable();
            $table->foreign('store_id')->references('id')->on('stores')->cascadeOnDelete();

            $table->boolean('is_active')->default(1);

            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('whatsappLink')->nullable();
            $table->string('instaLink')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches_content');
        Schema::dropIfExists('branches');
    }
};
