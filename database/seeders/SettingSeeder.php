<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Setting;
use App\Models\Language;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record=Setting::create([
            'created_at'=>now(),
            'updated_at'=>now(),
            'updateBy_id'=>1,
            'updateBy_type'=>'\App\Models\Admin',

        ]);

        $data=[];
        foreach (config('app.settings_translations') as $translation){
            foreach(Language::all() as $Language){
                foreach(Setting::all() as $Setting){
                    $lang= $Language->name;
                    $data[]=[
                        'language_id'=>$Language->id,
                        'setting_id'=>$record->id,
                        'content' => $$lang[array_rand($$lang)],
                        'type'=>$translation
                    ];
                }
            }
        }
    }
}
