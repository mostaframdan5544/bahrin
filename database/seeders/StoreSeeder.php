<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Store;
use App\Models\Image;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Store::factory()->count(100)->create();;  
        
        foreach(Store::all() as $ad){
            Image::create([
                'filename'=>'/uploads/Store-test.png',
                'imageable_type'=>'App\Models\Store',
                'imageable_id'=>$ad->id,
                'type'=>'main',
                'createBy_id'=>1,
                'createBy_type'=>'\App\Models\Admin',
    
            ]);
        } 
    }
}
