<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Notification;
use App\Models\NotificationContent;
use App\Models\Language;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            UserSeeder::class,
            SettingSeeder::class,
            LanguageSeeder::class,
            AdSeeder::class,
            NotificationSeeder::class,
            NotificationContentSeeder::class,
            NotifySeeder::class,
            BranchSeeder::class,
            BranchContentSeeder::class,
            CategorySeeder::class,
            CategoryContentSeeder::class,
            DistrictSeeder::class,
            DistrictContentSeeder::class,
            StoreSeeder::class,
            StoreContentSeeder::class,
        ]);

        
    }
}
