<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Notification;
use App\Models\Notify;
use App\Models\User;

class NotifySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Notify=[];
        foreach(Notification::all() as $Notification){
            $Notify[]=[
                'notification_id'=>$Notification->id,
                'user_id' => User::first()->id,
                'created_at'=>now()
            ];
        }
        Notify::insert($Notify);    
    }
}
