<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\CategoryContent;
use App\Models\Language;


class CategoryContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ar = [
            'مثال لقسم 1',
            'مثال لقسم 2',
            'مثال لقسم 3'
        ];
        $en = [
            'random test 1',
            'random test 2',
            'random test 3'
        ];
        $data=[];
        foreach (config('app.categories_translations') as $translation){

            foreach(Language::all() as $Language){
                foreach(Category::all() as $Category){
                    $lang= $Language->name;
                    $data[]=[
                        'language_id'=>$Language->id,
                        'category_id'=>$Category->id,
                        'content' => $$lang[array_rand($$lang)],
                        'type'=>$translation
                    ];
                }
            }
        }    
        CategoryContent::insert($data);
    }
}
