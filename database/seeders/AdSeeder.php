<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Ad;
use App\Models\Image;

class AdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ad::factory()->count(10)->create();;  
        foreach(Ad::all() as $ad){
            Image::create([
                'filename'=>'/uploads/abcdef.png',
                'imageable_type'=>'App\Models\Ad',
                'imageable_id'=>$ad->id,
                'type'=>'main',
                'createBy_id'=>1,
                'createBy_type'=>'\App\Models\Admin',
    
            ]);
        }  

    }
}
