<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\District;
use App\Models\DistrictContent;
use App\Models\Language;

class DistrictContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ar = [
            'مثال لحي 1',
            'مثال لحي 2',
            'مثال لحي 3'
        ];
        $en = [
            'random test 1',
            'random test 2',
            'random test 3'
        ];
        $data=[];
        foreach (config('app.districts_translations') as $translation){

            foreach(Language::all() as $Language){
                foreach(District::all() as $record){
                    $lang= $Language->name;
                    $data[]=[
                        'language_id'=>$Language->id,
                        'district_id'=>$record->id,
                        'content' => $$lang[array_rand($$lang)],
                        'type'=>$translation
                    ];
                }
            }    
        }    
        DistrictContent::insert($data);
    }
}
