<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Store;
use App\Models\StoreContent;
use App\Models\Language;

class StoreContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ar = [
            'مثال لمتجر 1',
            'مثال لمتجر 2',
            'مثال لمتجر 3'
        ];
        $en = [
            'random test 1 for store',
            'random test 2 for store',
            'random test 3 for store'
        ];
        $data=[];
        foreach (config('app.stores_translations') as $translation){

            foreach(Language::all() as $Language){
                foreach(Store::all() as $record){
                    $lang= $Language->name;
                    $data[]=[
                        'language_id'=>$Language->id,
                        'Store_id'=>$record->id,
                        'content' => $$lang[array_rand($$lang)],
                        'type' => $translation,
                    ];
                }
            }    
        }    
        StoreContent::insert($data);
    }
}
