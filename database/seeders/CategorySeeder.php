<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Image;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::factory()->count(100)->create();
        $sub = Category::orderBy('id','desc')->first();
        $sub->update([
            'category_id'=>$sub->id
        ]);    

        foreach(Category::all() as $ad){
            Image::create([
                'filename'=>'/uploads/Category-test.png',
                'imageable_type'=>'App\Models\Category',
                'imageable_id'=>$ad->id,
                'type'=>'main',
                'createBy_id'=>1,
                'createBy_type'=>'\App\Models\Admin',
            ]);
        }  
    }
}
