<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Branch;
use App\Models\BranchContent;
use App\Models\Language;

class BranchContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ar = [
            'مثال لفرع 1',
            'مثال لفرع 2',
            'مثال لفرع 3'
        ];
        $en = [
            'random test 1',
            'random test 2',
            'random test 3'
        ];
        $data=[];
        foreach (config('app.branches_translations') as $translation){

            foreach(Language::all() as $Language){
                foreach(Branch::all() as $record){
                    $lang= $Language->name;
                    $data[]=[
                        'language_id'=>$Language->id,
                        'branch_id'=>$record->id,
                        'content' => $$lang[array_rand($$lang)],
                        'type'=>$translation
                    ];
                }
            }    
        }    
        BranchContent::insert($data);
    }
}

