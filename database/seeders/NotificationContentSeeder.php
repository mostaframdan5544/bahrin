<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Notification;
use App\Models\NotificationContent;
use App\Models\Language;

class NotificationContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ar = [
            'مثال لاشعار 1',
            'مثال لاشعار 2',
            'مثال لاشعار 3'
        ];
        $en = [
            'random test 1',
            'random test 2',
            'random test 3'
        ];
        $NotificationContent=[];
        foreach (config('app.notifications_translations') as $translation){
            foreach(Language::all() as $Language){
                foreach(Notification::all() as $Notification){
                    $lang= $Language->name;
                    $NotificationContent[]=[
                        'language_id'=>$Language->id,
                        'notification_id'=>$Notification->id,
                        'content' => $$lang[array_rand($$lang)],
                        'type'=>$translation
                    ];
                }
            }
        }
        NotificationContent::insert($NotificationContent);    }
}
