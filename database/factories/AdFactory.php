<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ad>
 */
class AdFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "start_at"=>$this->faker->dateTimeBetween('-09 days', '-01 days'),
            "end_at"=>$this->faker->dateTimeBetween('now', '+10 days'),
            "created_at"=>now(),
            'createBy_id'=>1,
            'createBy_type'=>'\App\Models\Admin',

        ];
    }
}
