<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Store>
 */
class StoreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'created_at'=>now(),
            'category_id'=>Category::whereNotNull('category_id')->first()->id,
            'createBy_id'=>1,
            'createBy_type'=>'\App\Models\Admin',

        ];
    }
}
