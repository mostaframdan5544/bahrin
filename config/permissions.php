<?php

return [
    'modules' => [
        'admins' => [
            'name' => 'admins',
            'permissions' => 'c - r - u - d',
        ],
        'app_settings' => [
            'name' => 'app_settings',
            'permissions' => ' r - u - ',
        ],
        'notifications' => [
            'name' => 'notifications',
            'permissions' => 'c - r - u - d',
        ],
        'categories' => [
            'name' => 'categories',
            'permissions' => 'c - r - u - d',
        ],
        'districts' => [
            'name' => 'regions',
            'permissions' => 'c - r - u - d',
        ],
        'stores' => [
            'name' => 'stores',
            'permissions' => 'c - r - u - d',
        ],
        'branches' => [
            'name' => 'branches',
            'permissions' => 'c - r - u - d',
        ],
        'contacts' => [
            'name' => 'contacts',
            'permissions' => 'c - r - u ',
        ],
        'ads' => [
            'name' => 'ads',
            'permissions' => 'c - r - u ',
        ],
    ]
];
