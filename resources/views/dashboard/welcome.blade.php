@extends('dashboard.layouts.app')
@section('content')
<!-- Card -->
<div class="card">
   <!-- Product Details -->
   <div class="product-details pb-0">
      <div class="row">
         <div class="col-lg-8">
            <!-- Product Details Content -->
            <div class="product-details-content position-relative">
               <!-- Product Title -->
               <h1 >{{__('welcome back',['name'=>AuthLogged()->name])}}</h1>
               <!-- End Product Title -->

               <!-- Product Price -->
               <p class="price">
                  <span class="woocommerce-Price-amount amount">{{AuthLogged()->email}}</span>
               </p>
               <!-- End Product Price -->

            </div>
            <!-- End Product Details Content -->
         </div>
      </div>

   </div>
   <!-- End Product Details -->
</div>
<!-- End Card -->

@endsection