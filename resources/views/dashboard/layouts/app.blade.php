<!DOCTYPE html>
<html lang="zxx">

<head>
   <!-- Page Title -->
   <title>Dashmin - Multipurpose Bootstrap Dashboard Template</title>

   <!-- Meta Data -->
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta http-equiv="content-type" content="text/html; charset=utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="description" content="">
   <meta name="keywords" content="">

   <!-- Favicon -->
   <link rel="shortcut icon" href="{{asset('dashboard/assets/img/favicon.png')}}">

   @include('dashboard.inc.styles')

</head>

<body>

   <!-- Offcanval Overlay -->
   <div class="offcanvas-overlay"></div>
   <!-- Offcanval Overlay -->

   <!-- Wrapper -->
   <div class="wrapper">
   
      @include('dashboard.inc.navbar')

      <!-- Main Wrapper -->
      <div class="main-wrapper">
         @include('dashboard.inc.sidebar')
         
         <div class="main-content">

            <div class="container-fluid">

               @yield('content')

            </div>

         </div>

      </div>
      <!-- End Main Wrapper -->

      @include('dashboard.inc.footer')

   </div>
   <!-- End wrapper -->

   @include('dashboard.inc.scripts')
   @include('dashboard.inc.delete_record')
   @stack('scripts')

</body>

</html>