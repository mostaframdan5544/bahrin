 <!-- Sidebar -->
 @php $routeName = \Request::route()->getName(); @endphp
 <nav class="sidebar" data-trigger="scrollbar">
   <!-- Sidebar Header -->
   <div class="sidebar-header d-none d-lg-block">
      <!-- Sidebar Toggle Pin Button -->
      <div class="sidebar-toogle-pin">
         <i class="icofont-tack-pin"></i>
      </div>
      <!-- End Sidebar Toggle Pin Button -->
   </div>
   <!-- End Sidebar Header -->

   <!-- Sidebar Body -->
   <div class="sidebar-body">
      <!-- Nav -->
      <ul class="nav">
         <li class="nav-category">@lang('Main')</li>
         <li class="{{$routeName != 'dashboard.welcome' ?: 'active'}}">
            <a href="{{route('dashboard.welcome')}}">
               <i class="icofont-pie-chart"></i>
               <span class="link-title">{{__('home')}}</span>
            </a>
         </li>
         
         <li class="{{$routeName != 'dashboard.statistics' ?: 'active'}}">
            <a href="{{route('dashboard.statistics')}}">
               <i class="icofont-chart-histogram"></i>
               <span class="link-title">{{__('statistics')}}</span>
            </a>
         </li>
         <li class="nav-category">{{__('application')}}</li>
         <li  class="{{  collect(['categories.index','categories.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('Categories')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li><a href="{{route('categories.index')}}">{{__('all')}}</a></li>
               <li><a href="{{route('categories.create')}}">{{__('create')}}</a></li>
               <li class="{{collect([Request::fullUrl()])->contains(url()->current().'?trashOnly=true' ) ? 'active':''}}"><a href="{{route('categories.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>
         
         <li  class="{{  collect(['categories.index','categories.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('Sub Categories')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li><a href="{{route('categories.index')}}?subCategories=true">{{__('all')}}</a></li>
               <li><a href="{{route('categories.create')}}?subCategories=true">{{__('create')}}</a></li>
               <li class="{{collect([Request::fullUrl()])->contains(url()->current().'?trashOnly=true' ) ? 'active':''}}"><a href="{{route('categories.index')}}?trashOnly=true&subCategories=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>


         

         <li  class="{{  collect(['stores.index','stores.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('Stores')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li  class="{{  collect(['stores.index','stores.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
                  <a href="{{route('stores.index')}}">{{__('all')}}</a>
               </li>
               <li  class="{{  collect(['stores.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
                  <a href="{{route('stores.create')}}">{{__('create')}}</a>
               </li>
               <li class="{{collect([Request::fullUrl()])->contains(url()->current().'?trashOnly=true' ) ? 'active':''}}"><a href="{{route('stores.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>



         <li  class="{{  collect(['branches.index','branches.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('branches')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li  class="{{  collect(['branches.index','branches.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
                  <a href="{{route('branches.index')}}">{{__('all')}}</a>
               </li>
               <li  class="{{  collect(['branches.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
                  <a href="{{route('branches.create')}}">{{__('create')}}</a>
               </li>
               <li class="{{collect([Request::fullUrl()])->contains(url()->current().'?trashOnly=true' ) ? 'active':''}}"><a href="{{route('stores.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>         <li  class="{{  collect(['districts.index','districts.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('regions')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li><a href="{{route('districts.index')}}">{{__('all')}}</a></li>
               <li><a href="{{route('districts.create')}}">{{__('create')}}</a></li>
               <li class="{{collect([Request::fullUrl()])->contains(url()->current().'?trashOnly=true' ) ? 'active':''}}"><a href="{{route('districts.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>

         <li  class="{{  collect(['ads.index','ads.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('ads')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li><a href="{{route('ads.index')}}">{{__('all')}}</a></li>
               <li><a href="{{route('ads.create')}}">{{__('create')}}</a></li>
               <li class="{{collect([Request::fullUrl()])->contains(url()->current().'?trashOnly=true' ) ? 'active':''}}"><a href="{{route('ads.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>

         <li class="nav-category">{{__('contacts')}}</li>
         
         <li class="{{  collect(['contacts.index'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('Contacts & Complains')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
               <li class="{{$routeName != 'contacts.index' ?: 'active'}}"><a href="{{route('contacts.index')}}">{{__('all')}}</a></li>
               <li class="{{Str::contains(request()->fullUrl(),route('contacts.index')) && Str::contains(request()->fullUrl(),'trash') 
                  ? 'active':''}} "><a href="{{route('contacts.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>

         <li class="{{  collect(['notifications.index','notifications.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('Notifications')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
               <li class="{{$routeName != 'notifications.index' ?: 'active'}}"><a href="{{route('notifications.index')}}">{{__('all')}}</a></li>
               <li class="{{$routeName != 'notifications.create' ?: 'active'}}"><a href="{{route('notifications.create')}}">{{__('create')}}</a></li>
               <li class="{{Str::contains(request()->fullUrl(),route('notifications.index')) && Str::contains(request()->fullUrl(),'trash') 
                  ? 'active':''}} "><a href="{{route('notifications.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>

         <li class="nav-category">{{ __('dashboard')}}</li>

         <li class="{{  collect(['admins.index','admins.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('Admins')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li><a href="{{route('admins.index')}}">{{__('all')}}</a></li>
               <li><a href="{{route('admins.create')}}">{{__('create')}}</a></li>
               <li class="{{collect([Request::fullUrl()])->contains(url()->current().'?trashOnly=true' ) ? 'active':''}}"><a href="{{route('admins.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>

         <li class="{{  collect(['roles.index','roles.create'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('Roles')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li class="{{$routeName != 'roles.index' ?: 'active'}}"><a href="{{route('roles.index')}}">{{__('all')}}</a></li>
               <li><a href="{{route('roles.create')}}">{{__('create')}}</a></li>
               <li class="{{Str::contains(request()->fullUrl(),route('roles.index')) && Str::contains(request()->fullUrl(),'trash') 
                  ? 'active':''}} "><a href="{{route('roles.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>

         <li class="nav-category">{{__('Apsettings')}}</li>

         <li class="{{  collect(['app_settings.index'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="{{route('app_settings.index')}}">
               <i class="icofont-magic-alt"></i>
               <span class="link-title">{{__('Apsettings')}}</span>
            </a>
         </li>

         <li class="{{  collect([route('admins.show',AuthLogged()->id??0)])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="{{route('admins.show',AuthLogged()->id??'0')}}">
               <i class="icofont-magic-alt"></i>
               <span class="link-title">{{__('My Profile')}}</span>
            </a>
         </li>
         <li class="{{  collect(['resetPassword.index'])->contains(request()->route()->getName() )?'active sub-menu-opened' : ''  }}">
            <a href="{{route('resetPassword.index')}}">
               <i class="icofont-magic-alt"></i>
               <span class="link-title"> {{__('resetPassword')}}</span>
            </a>
         </li>

         <li  >
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('Roles')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
            <li class="{{$routeName != 'roles.index' ?: 'active'}}"><a href="{{route('roles.index')}}">{{__('all')}}</a></li>
               <li><a href="{{route('roles.create')}}">{{__('create')}}</a></li>
               <li class="{{Str::contains(request()->fullUrl(),route('roles.index')) && Str::contains(request()->fullUrl(),'trash') 
                  ? 'active':''}} "><a href="{{route('roles.index')}}?trashOnly=true">{{__('Recycle')}}</a></li>
            </ul>
            <!-- End Sub Menu -->
         </li>


         <li>
            <a href="#">
               <i class="icofont-mail-box"></i>
               <span class="link-title">{{__('change lang')}}</span>
            </a>
            <!-- Sub Menu -->
            <ul class="nav sub-menu">
               @foreach(languages() as $lang)
                  <li >
                     <a href="{{route('dashboard.changeLnag',$lang->name)}}?lang={{$lang->name}}">
                        <span><img src="{{asset('dashboard/assets/img/usa.png')}}" alt=""></span>
                        {{__($lang->name)}}
                     </a>
                  </li>
               @endforeach
            </ul>
            <!-- End Sub Menu -->
         </li>

      </ul>
      <!-- End Nav -->
   </div>
   <!-- End Sidebar Body -->
</nav>
<!-- End Sidebar -->
