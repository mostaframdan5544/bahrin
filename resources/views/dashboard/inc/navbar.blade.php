<!-- Header -->
<header class="header white-bg fixed-top d-flex align-content-center flex-wrap">
    <!-- Logo -->
    <div class="logo">
    <a href="{{route('dashboard.welcome')}}" class="default-logo"><img src="{{asset('dashboard/assets/img/logo.png')}}" alt=""></a>
    <a href="{{route('dashboard.welcome')}}" class="mobile-logo"><img src="{{asset('dashboard/assets/img/mobile-logo.png')}}" alt=""></a>
    </div>
    <!-- End Logo -->

    <!-- Main Header -->
    <div class="main-header">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-3 col-lg-1 col-xl-4">
                <!-- Header Left -->
                <div class="main-header-left h-100 d-flex align-items-center">
                

                <!-- Main Header Menu -->
                <div class="main-header-menu d-block d-lg-none">
                    <div class="header-toogle-menu">
                        <!-- <i class="icofont-navigation-menu"></i> -->
                        <img src="{{asset('dashboard/assets/img/menu.png')}}" alt="">
                    </div>
                </div>
                <!-- End Main Header Menu -->
                </div>
                <!-- End Header Left -->
            </div>
            <div class="col-9 col-lg-11 col-xl-8">
                <!-- Header Right -->
                <div class="main-header-right d-flex justify-content-end">
                    <ul class="nav">

                        <li class="ml-0">
                            <!-- Main Header Language -->
                            <div class="main-header-language">
                                <a href="#" data-toggle="dropdown">
                                <img src="{{asset('dashboard/assets/img/svg/globe-icon.svg')}}" alt="">
                                </a>
                                <div class="dropdown-menu style--three">
                                    @foreach(languages() as $lang)
                                        <a href="{{route('dashboard.changeLnag',$lang->name)}}?lang={{$lang->name}}">
                                            <span><img src="{{asset('dashboard/assets/img/usa.png')}}" alt=""></span>
                                            {{__($lang->name)}}
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                            <!-- End Main Header Language -->
                        </li>
                        
                        <li class="d-none d-lg-flex">
                            <!-- Main Header Time -->
                            <div class="main-header-date-time text-right">
                                <h3 class="time">
                                <span >{{date('i')}}</span>
                                <span >:</span>
                                <span >{{date('H')}}</span>
                                </h3>
                                <span class="date"><span id="date">Tue, 12 October 2019</span></span>
                            </div>
                            <!-- End Main Header Time -->
                        </li>

                        @if(AuthLogged())
                            <li class="d-none d-lg-flex">
                                <!-- Main Header Button -->
                                <div class="main-header-btn ml-md-1">
                                    <a  href="{{ route('dashboard.logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();"
                                        class="btn">{{__('logout')}}
                                    </a>
                                    <form id="logout-form" action="{{ route('dashboard.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                                <!-- End Main Header Button -->
                            </li>
                        @endif
                        
                    </ul>
                </div>
                <!-- End Header Right -->
            </div>
        </div>
    </div>
    </div>
    <!-- End Main Header -->
</header>
<!-- End Header -->