
   <!-- Web Fonts -->
   <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&display=swap" rel="stylesheet">
   
   <!-- ======= BEGIN GLOBAL MANDATORY STYLES ======= -->
   <link rel="stylesheet" href="{{asset('dashboard/assets/bootstrap/css/bootstrap.min.css')}}">
   <link rel="stylesheet" href="{{asset('dashboard/assets/fonts/icofont/icofont.min.css')}}">
   <link rel="stylesheet" href="{{asset('dashboard/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.css')}}">
   <!-- ======= END BEGIN GLOBAL MANDATORY STYLES ======= -->
   
   <!-- ======= BEGIN PAGE LEVEL PLUGINS STYLES ======= -->
   <link rel="stylesheet" href="{{asset('dashboard/assets/plugins/apex/apexcharts.css')}}">
   <!-- ======= END BEGIN PAGE LEVEL PLUGINS STYLES ======= -->

   <link rel="stylesheet" href="{{asset('dashboard/assets/fonts/pe-icon-7-stroke/pe-icon-7-stroke.css')}}">
   <link rel="stylesheet" href="{{asset('dashboard/assets/plugins/datepicker/datepicker.min.css')}}">
   <link rel="stylesheet" href="{{asset('dashboard/assets/plugins/timepicker/jquery.timepicker.min.css')}}">
   <link rel="stylesheet" href="{{asset('dashboard/assets/plugins/daterangepicker/daterangepicker.css')}}">


   @stack('style')
   <!-- ======= MAIN STYLES ======= -->
   <link rel="stylesheet" href="{{asset('dashboard/assets/css/style.css')}}">
   <!-- ======= END MAIN STYLES ======= -->

