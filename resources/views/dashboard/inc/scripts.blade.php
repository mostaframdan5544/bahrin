  <!-- ======= BEGIN GLOBAL MANDATORY SCRIPTS ======= -->
  <script src="{{asset('dashboard/assets/js/jquery.min.js')}}"></script>
   <script src="{{asset('dashboard/assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
   <script src="{{asset('dashboard/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
   <script src="{{asset('dashboard/assets/js/script.js')}}"></script>
   <!-- ======= BEGIN GLOBAL MANDATORY SCRIPTS ======= -->

   <!-- ======= BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS ======= -->
   <script src="{{asset('dashboard/assets/plugins/apex/apexcharts.min.js')}}"></script>
   <script src="{{asset('dashboard/assets/plugins/apex/custom-apexcharts.js')}}"></script>
   <!-- ======= End BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS ======= -->

      <!-- ======= BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS ======= -->
      <script src="{{asset('dashboard/assets/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
   <script src="{{asset('dashboard/assets/plugins/moment/moment.min.js')}}"></script>
   <script src="{{asset('dashboard/assets/plugins/datepicker/datepicker.min.js')}}"></script>
   <script src="{{asset('dashboard/assets/plugins/timepicker/jquery.timepicker.min.js')}}"></script>
   <script src="{{asset('dashboard/assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
   
   <script src="{{asset('dashboard/assets/plugins/datepicker/i18n/datepicker.en.js')}}"></script>
   <script src="{{asset('dashboard/assets/plugins/datepicker/i18n/datepicker-ar.js')}}"></script>
   
   <script src="{{asset('dashboard/assets/plugins/datepicker/custom-form-datepicker.js')}}"></script>
   <!-- ======= End BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS ======= -->

   <script src="{{asset('dashboard/assets/plugins/blockUI/jquery.blockUI.js') }}"></script>
    <script src="{{asset('dashboard/assets/plugins/blockUI/custom-blockUi.js')}}"></script>

    
    @include('dashboard.inc.searchAjax')
    @include('dashboard.inc.ajaxFunction')
    @stack('script')

  <!-- <script src="{{asset(Config::get('app.locale').'/assets/js/scrollspyNav.js')}}"></script> -->

  <!-- END GLOBAL MANDATORY SCRIPTS -->
<script>
    $("body").on("click",'.switch input',function(e){
        let data_url = $(this).data('url');
        let url;
        url =
        data_url?data_url:`{{route('dashboard.changeStatus',[':model',':col_name',':id'])}}`
        let model= $(this).data('model');
        let col_name= $(this).data('col_name');
        let id= $(this).data('id');
        url = url.replace(':model',model).replace(':col_name',col_name).replace(':id',id);
        $.ajax({
            url,
            type: 'GET',
            cache: false,
            contentType: false,
            processData: false,
            error: function(response) {
                alert('error');
            }
        });
    });
</script>
@include('dashboard.buttons.multi-deleted')
@include('dashboard.buttons.multi-restore')
