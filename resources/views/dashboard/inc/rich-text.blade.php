<textarea id="{{$name}}" name="{{$name}}">
        {!! $value !!}
</textarea>
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( '{{$name}}' );
</script>