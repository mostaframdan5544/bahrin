<div class="upload-avatar d-xl-flex align-items-center flex-column">

    <div>
        <div class="attach-file style--two rounded-0 align-items-end mb-3">
            <img src="{{asset($current_image_path??'dashboard/assets/img/img-placeholder.png')}}" class="profile-avatar" alt="">
            <div class="upload-button mb-20">
                <img src="{{asset('dashboard/assets/img/svg/gallery.svg')}}" alt="" class="svg mr-2">
                <span>{{__('Upload a Photo')}}</span>
                <input class="file-input" name="{{$name}}" type="file" id="fileUpload" accept="image/*">
            </div>
            </div>

            <div class="content">
            <h4 class="mb-2">{{__('Upload a Photo')}}</h4>
            <p class="font-12 c4"> JPG, GIF or PNG. Max size <br /> of 800kB</p>   
            </div>
    </div>

    @error($name)
        <div class="invalid-feedback">
            {{$message}}
        </div>
    @enderror
</div>