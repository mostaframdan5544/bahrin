<div class="d-flex align-items-center justify-content-end mt-3 mt-sm-0">
            
    <!-- Delete Mail -->
    <div class="delete_mail ">
        <a href="#"
            data-toggle="modal"
            data-target="#multi-delete-modal"
            data-model="{{$module->routeNamePrefix}}"
            data-toggle="tooltip"
            class="bg-danger btn btn-outline-primary box-shadow-3 mr-1 mb-1 d-none multi-delete-record-button" data-placement="top" title="Edit">
            <span class="multi-delete-record-count"></span>
                <img style="height:30px !important;width:60px !important" src="{{asset('dashboard/assets/img/svg/delete.svg')}}" alt="" class="svg">
        </a>
    </div>
    <!-- End Delete Mail -->

</div>