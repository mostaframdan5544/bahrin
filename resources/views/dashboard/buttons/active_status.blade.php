
<label class="switch with-icon">
    <input type="checkbox" @if($record->$col_name) checked @endif data-model="{{$model}}" data-col_name="{{$col_name}}" data-id="{{$record->id}}"
        data-url="">
    <span class="control">
        <span class="check"></span>
    </span>
</label>