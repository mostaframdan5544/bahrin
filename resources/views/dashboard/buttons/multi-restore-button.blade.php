
@if( Str::contains(request()->fullUrl(),'trashOnly=true'))
    <div class="d-flex align-items-center justify-content-end mt-3 mt-sm-0">
        
        <!-- Delete Mail -->
        <div id="restore"
            data-toggle="modal"
            data-target="#multi-restore-modal"
            data-model="{{$module->routeNamePrefix}}"
            data-toggle="tooltip"
            class=" bg-success btn btn-outline-primary box-shadow-3 mr-1 mb-1 d-none multi-restore-record-button" data-placement="top" title="Restore">
            <i class="pe-7s-refresh"></i>
            <span class="icon__name">{{__('restore')}} <span class="display-6" id="restore-count">1</span></span>
        </div>
        
    </div>
    @endif