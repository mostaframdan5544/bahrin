<!DOCTYPE html>
<html lang="zxx">

<head>
    <!-- Page Title -->
    <title>{{config('app.name')}}</title>

    <!-- Meta Data -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">

    @include('dashboard.inc.styles')

</head>

<body>
 
   
    @include('dashboard.inc.navbar')
    <div class="mn-vh-100 d-flex align-items-center">
        <div class="container">
            <!-- Card -->
            <div class="card justify-content-center auth-card">
                <div class="row justify-content-center">
                    <div class="col-xl-7 col-lg-9">
                        <h4 class="mb-5 font-20">{{__('welcome')}}</h4>

                        <form method="POST" action="{{ route('dashboard.login') }}" class="text-left form" >
                            <!-- Form Group -->
                            @csrf
                            <div class="form-group mb-20">
                                <label for="email" class="mb-2 font-14 bold black">{{__('user_name')}}</label>
                                <input type="email" id="email" class="theme-input-style" name="username" placeholder="Email Address">
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <!-- End Form Group -->
                            
                            <!-- Form Group -->
                            <div class="form-group mb-20">
                                <label for="password" class="mb-2 font-14 bold black">{{__('password')}}</label>
                                <input type="password" name="password" id="password" class="theme-input-style" placeholder="********">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <!-- End Form Group -->

                            <div class="d-flex justify-content-between mb-20">
                                <div class="d-flex align-items-center">
                                    <!-- Custom Checkbox -->
                                    <label class="custom-checkbox position-relative mr-2">
                                        <input type="checkbox" name="remember_token" id="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                    <!-- End Custom Checkbox -->
                                    
                                    <label for="checkbox" class="font-14">{{__('Keep_me_logged_in')}}</label>
                                </div>

                            </div>

                            <div class="mb-30 d-none">
                                <a href="#" class="light-btn mr-3 mb-20">Log In With Facebook</a>
                                <a href="#" class="light-btn style--two mb-20">Log In With Gmail</a>
                            </div>
                            @include('dashboard.inc.session')
                            <div class="d-flex align-items-center">
                                <button type="submit" class="btn long mr-20">{{ __('login')}}</button>
                            </div>
                        </form>
                    </div>                                    
                </div>
            </div>
            <!-- End Card -->
        </div>
    </div>

    @include('dashboard.inc.footer')
    @include('dashboard.inc.scripts')
</body>

</html>