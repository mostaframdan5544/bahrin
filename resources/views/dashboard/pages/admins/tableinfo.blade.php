@foreach($records as $record)
    <tr>
        <td class="checkbox-column">
            <div class="custom-control custom-checkbox checkbox-primary">
                <input type="checkbox" class="custom-control-input todo" id="todo-{{$record->id}}" name="ids_selected[]" value="{{$record->id}}">
                <label class="custom-control-label" for="todo-{{$record->id}}"></label>
            </div>
        </td>         
        @foreach($columns as $column)
            <td>{{$record->{$column} }}</td>
        @endforeach
        <td>
            @foreach($module->togglableColumns as $togglableColumns)
                @include(config('app.dashboardFolder').'.buttons.active_status',['record'=>$record,'col_name'=>$togglableColumns,'model'=>$model])
            @endforeach
        </td>
        <td>
            @if($record->createByAuth)
                <a href="{{route('admins.show',$record->createByAuth->id)}}" target="_blank" rel="noopener noreferrer">{{$record->createByAuth->name}}</a>
            @endif
        </td>   
        <td >
            @php $destroy= AuthLogged()->id == $record->id?'':'destroy' @endphp
            @include(config('app.dashboardFolder').'.buttons.actions',['routeNamePrefix'=>$routeNamePrefix,'type'=>'edit-'.$destroy.'-show'])
        </td>
    </tr>
@endforeach
