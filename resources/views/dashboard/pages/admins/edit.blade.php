@extends(config('app.dashboardFolder').'layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="card">

            <div class="card-body">

                <div class="col-lg-12">
                    <!-- Base Horizontal Form -->
                    <div class="form-element py-30 mb-30">
                        <h4 class="font-20 mb-4">@lang('update')</h4>

                        <!-- Form -->
                        <form method="POST" action="{{route($module->routeNamePrefix.'update',$record->id)}}">
                            @csrf
                            @method("PUT")
                            <input type="hidden" name="id" value="{{$record->id}}">
                            <div class=" form-row mb-20">
                                <label for="role" class="mb-2 black bold">{{__('role')}}</label>
                                <select class="theme-input-style @error('role_id') is-invalid @enderror" name="role_id" id="exampleSelect6">
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}" @selected($record->role_id== $role->id)>{{$role->name}}</option>
                                    @endforeach
                                </select>
                                @error('role_id')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-row mb-20">
                                <label class="font-14 bold">{{__('name')}}</label>
                                <input type="text" value="{{$record->name}} " class=" @error('name') is-invalid @enderror theme-input-style @error('name') is-invalid @enderror"  name="name" placeholder="{{__('enter here')}}">
                                @error('name')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <hr>
                            <div class="form-group mb-4 form-row">
                                <label for="addonEmail2" class="mb-2 font-14 black bold">{{__('email')}}</label>

                                <div class="input-group addon">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <img src="{{asset('dashboard/assets/img/svg/unread.svg')}}" alt="" class="svg">
                                        </div>
                                    </div>
                                    <input type="text" value="{{$record->email}}" id="addonEmail2" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="{{__('enter here')}}">
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group  mb-4">
                                <label for="telInput1" class="mb-2 black bold">{{__('phone')}}</label>
                                <input type="tel"  value="{{$record->phone}}" class="theme-input-style @error('phone') is-invalid @enderror" id="telInput1" name="phone" placeholder="{{__('enter here')}}">
                                @error('phone')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-4">
                                <label for="password" class="mb-2 black bold">{{__('password')}}</label>
                                <input type="password" class="theme-input-style @error('password') is-invalid @enderror" id="password" name="password" placeholder="{{__('enter here')}}">
                                @error('password')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-4">
                                <label for="password_confirmation" class="mb-2 black bold">{{__('password_confirmation')}}</label>
                                <input type="password" class="theme-input-style @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="{{__('enter here')}}">
                                @error('password_confirmation')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <!-- Form Row -->
                            <div class="form-row ">
                                <div class="col-12 ">
                                    <button type="submit" class="btn long mt-5"> @lang('Save') </button>
                                </div>
                            </div>
                            <!-- End Form Row -->
                        </form>
                        <!-- End Form -->
                    </div>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $("body").on("input","input[name='all']",function(){
                $("input[type='checkbox']").attr('checked',$(this).is(':checked'))
            });
        </script>
    @endpush
@endsection
