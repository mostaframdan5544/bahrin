@extends(config('app.dashboardFolder').'layouts.app')


@section('content')

    <div class="content-wrapper">
        <div class="card-body">
            <table class="table table-striped gy-7 gs-7">
                <thead>

                </thead>
                <tbody>
                <tr>
                        <td class="text-capitalize font-weight-bold">@lang('name')</td>
                        <td>
                        {{$record->name}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td class="text-capitalize font-weight-bold">@lang('email')</td>
                        <td>
                        {{$record->email}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>


                    <tr>
                        <td class="text-capitalize font-weight-bold">@lang('phone')</td>
                        <td>
                        {{$record->phone}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    @if($record->role)
                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('role')</td>
                            <td>
                            {{$record->role->name}}
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif
                    

                    @if($record->createByAuth)
                    <tr>
                        
                    <td class="text-capitalize font-weight-bold">@lang('created by')</td>
                        
                        <td>
                            <a href="{{route('admins.show',$record->createByAuth->id)}}" target="_blank" rel="noopener noreferrer">{{$record->createByAuth->name}}</a>
                        </td>
                    </tr>
                    @endif
                    @if($record->updated_at)

                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('updated at')</td>
                            <td>
                                {{$record->updated_at}}
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif

                    @if($record->updateByAuth)
                    <tr>
                        
                    <td class="text-capitalize font-weight-bold">@lang('updated by')</td>
                        
                        <td>
                            <a href="{{route('admins.show',$record->updateByAuth->id)}}" target="_blank" rel="noopener noreferrer">{{$record->createByAuth->name}}</a>
                        </td>
                    </tr>
                    @endif

                    
                </tbody>
            </table>
        </div>
    </div>
@endsection
