<div class="row ">
    <div class="col-md-12 filtered-list-search">
        <!-- Card -->
        <!-- <form > -->
        <form method="GET" class="search-form flex-grow filters-form">
           <input type="hidden" name="subCategories" value="{{request()->query->all()['subCategories']??false}}" >
            <div class="card">
                <div class="form-element py-30">
                    <h3 class="font-20 mb-3 text-transform-none">{{__('filters')}}</h3>
                    <div class="row">
                    <div class="col-lg-3 col-xl-12">
                            <!-- Form Group -->
                            <div class="form-group mb-0"> 
                                <label class="mb-2 font-14 bold">{{__('from')}}</label>
                                <!-- Date Picker -->
                                <div class="dashboard-date style--four">
                                    <input type="date" name="from_created_at" value="{{request()->from_created_at}}" />
                                </div>
                                <!-- End Date Picker -->
                            </div>
                            <!-- End Form Group -->
                        </div>
                        <div class="col-lg-3 col-xl-12">
                            <!-- Form Group -->
                            <div class="form-group mb-0"> 
                                <label class="mb-2 font-14 bold">{{__('to')}}</label>
                                <!-- Date Picker -->
                                <div class="dashboard-date style--four">
                                    <input type="date" name="to_created_at" value="{{request()->to_created_at}}" />
                                </div>
                                <!-- End Date Picker -->
                            </div>
                            <!-- End Form Group -->
                        </div>
                        <div class="col-lg-3 col-xl-12">
                            <div class="form-group ">
                                <label for="exampleSelect1" class=" black bold d-block">@lang('total_result')</label>
                                <div class="custom-select style--two">
                                    <select class="theme-input-style" name="itemsPerPage" >
                                        <option disabled>@lang('total_result')</option>
                                        <option value="10" @selected(request()->itemsPerPage==10)>10</option>
                                        <option value="20" @selected(request()->itemsPerPage==20) >20</option>
                                        <option value="50" @selected(request()->itemsPerPage==50) >50</option>
                                        <option value="100" @selected(request()->itemsPerPage==100) >100</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-12">
                            <div class="form-group ">
                                <label for="exampleSelect1" class=" black bold d-block">@lang('total_result')</label>
                                <div class="custom-select style--two">
                                    <select class="theme-input-style" name="sortBy" >
                                        <option disabled>@lang('sort_by')</option>
                                        @foreach($module->getColumns() as $col)
                                            <option value="{{$col}}" @selected(request()->sortBy==$col) >{{__(''.$col)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-12">
                            <div class="form-group ">
                                <label for="exampleSelect1" class=" black bold d-block">@lang('total_result')</label>
                                <div class="custom-select style--two">
                                    <select class="theme-input-style" name="sortType" >
                                        <option disabled>@lang('sort_type')</option>
                                        <option value="asc" @selected(request()->sortType=='asc')>@lang('asc') </option>
                                        <option value="desc" @selected(request()->sortType=='desc')>@lang('desc') </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Card -->
            <!-- Contact Header -->
            <div class="contact-header d-flex align-items-sm-center media flex-column flex-sm-row bg-white mb-30">
                
            <div class="contact-header-left media-body d-flex align-items-center mr-4">
                <!-- Add New Contact Btn -->
                <div class="add-new-contact ">
                    <a href="{{ route($module->routeNamePrefix.'create') }}" class="btn btn-primary float-end ">
                        <img src="{{asset('dashboard/assets/img/svg/plus_white.svg')}}" alt="" class="svg mr-2">
                        @lang('create')
                    </a>
                    <!-- <a href="#" class="btn-circle" data-toggle="modal" data-target="#contactAddModal">
                        <img src="{{asset('dashboard/assets/img/svg/plus_white.svg')}}" alt="" class="svg">
                    </a> -->
                </div>
                <!-- End Add New Contact Btn -->

                <!-- Search Form -->
                <form action="#" class="search-form flex-grow">
                    <div class="theme-input-group style--two">
                        <input  class="theme-input-style searchAjax"
                            type="search" autofocus data-ajaxfunction='getRecords'
                            placeholder="@lang('search')" value="{{request()->keyword}}"
                            name='keyword'>

                            <button type="submit"><img src="{{asset('dashboard/assets/img/svg/search-icon.svg')}}" alt=""
                            class="svg"></button>
                    </div>
                </form>
                <!-- End Search Form -->
            </div>    

            @include(config('app.dashboardFolder').'.buttons.multi-delete-button')
            @include(config('app.dashboardFolder').'.buttons.multi-restore-button')
            </div>
            
        </form>

        <!-- </form>  -->
        </div>
        <!-- End Contact Header -->
</div>

<div class="table-responsive">
    
    <table class="contact-list-table text-nowrap bg-white">
        <thead>
            <tr>
                <th class="checkbox-column">
                    <div class="custom-control custom-checkbox checkbox-primary">
                        <input type="checkbox" class="custom-control-input todochkbox" id="todoAll">
                        <label class="custom-control-label" for="todoAll"></label>
                    </div>
                </th>
                @foreach($columns as $th)
                    <th>{{__("{$th}")}}</th>
                @endforeach
                @foreach($module->togglableColumns as $togglableColumns)
                    <th>{{__($togglableColumns)}}</th>
                @endforeach
                <th>{{__('created by')}}</th>

                <th >@lang('actions')</th>
            </tr>
        </thead>
        <tbody>
            {!! $module->tableInfo() !!}
        </tbody>
    </table>
    <div class="row mt-4">
        <div class="col-sm-6 offeset-sm-5 ">
             </div class="text-center">
                {!! $module->paginationHtml() !!}
            </div>
        </div>
    </div>
</div>
