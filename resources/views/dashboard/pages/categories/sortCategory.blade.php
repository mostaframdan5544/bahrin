@extends(config('app.dashboardFolder').'layouts.app')

@section('content')

    <div class="widget-content widget-content-area br-6">
        <div class="row">
            <div class="col-12">
                <!-- Card -->
                <div class="card bg-transparent">
                    <!-- /.session-messages -->
                    <div class="col-xl-6  offset-xl-1 pt-2 order-0 order-xl-1">
                            <h4 class="font-20 mb-30">Multiple Lists</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- Card -->
                                        <div class="card">
                                            <div class="mb-20">
                                                <h5>Team 01</h5>
                                            </div>

                                            <div class="dragable-team">
                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m6.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Steve Hughes</h4>
                                                        <span class="font-12">Principal Mobility Executive</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m30.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Steve Hughes</h4>
                                                        <span class="font-12">Customer Creative Consultant</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m11.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Steve Hughes</h4>
                                                        <span class="font-12">Lead Security Engineer</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m36.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Steve Hughes</h4>
                                                        <span class="font-12">Lead Factors Developer</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m37.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Steve Hughes</h4>
                                                        <span class="font-12">Regional Brand Analyst</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m15.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Steve Hughes</h4>
                                                        <span class="font-12">Forward Creative Administrator</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->
                                            </div>
                                        </div>
                                        <!-- End Card -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- Card -->
                                        <div class="card mt-5 mt-md-0">
                                            <div class="mb-20">
                                                <h5>Team 02</h5>
                                            </div>

                                            <div class="dragable-team">
                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m5.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Vito Kerluke DDS</h4>
                                                        <span class="font-12">National Implementation Director</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m31.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Dahlia Barrows</h4>
                                                        <span class="font-12">Corporate Group Engineer</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m12.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Jevon Franecki</h4>
                                                        <span class="font-12">Customer Operations Supervisor</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m39.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Madilyn Boehm</h4>
                                                        <span class="font-12">Senior Usability Facilitator</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m38.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Anita Lang</h4>
                                                        <span class="font-12">Product Communications</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->

                                                <!-- Team Member Item -->
                                                <div class="d-flex align-items-center">
                                                    <div class="img mr-3">
                                                        <img src="../../assets/img/avatar/m16.png" class="img-50" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h4>Magdalena Stroman</h4>
                                                        <span class="font-12">Future Data Facilitator</span>
                                                    </div>
                                                </div>
                                                <!-- End Team Member -->
                                            </div>
                                        </div>
                                        <!-- End Card -->
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </div>
@endsection
