@extends(config('app.dashboardFolder').'layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="card">

            <div class="card-body">

                <div class="col-lg-12">
                    <!-- Base Horizontal Form -->
                    <div class="form-element py-30 mb-30">
                        <h4 class="font-20 mb-4">@lang('edit')</h4>

                        <!-- Form -->
                        <form method="POST" action="{{route($module->routeNamePrefix.'update',$record->id)}}" enctype="multipart/form-data" >
                            @csrf
                            @method("PUT")

                            <div class=" form-row mb-20">
                                <label for="category" class="mb-2 black bold">{{__('category')}}</label>
                                <select class="theme-input-style @error('category_id') is-invalid @enderror" name="store_id" id="exampleSelect6">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @selected(old('category_id')== $category->id)>{{$category->content}}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>

                            @foreach(languages() as $lang)
                                <div class="billing-fields mt-5 mt-lg-0 border border-5 border-primary p-5">
                                    <h4 class="mb-4 font-20">{{__($lang->name)}}</h4>
                                        @foreach($module->translations as $translation)
                                        <div class="form-row mb-20">
                                            <label class="font-14 bold">{{__($translation)}}</label>
                                            <input type="text" value="{{$record->getContent($lang,$translation)}} " class="theme-input-style @error($translation.'_'.$lang->name) is-invalid @enderror"  name="{{$translation.'_'.$lang->name}}"placeholder="{{__('enter here')}}">
                                            @error($translation.'_'.$lang->name)
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                            @enderror
                                            
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            
                            <div class=" form-row mb-20 mt-20">

                                @include(config('app.dashboardFolder').'inc.upload-image',[
                                    'name'=>'image',
                                    'current_image_path'=>$record->image->filename??null   
                                ])
                            </div>
                            <br>
                            <hr>
                            <!-- Form Row -->
                            <div class="form-row ">
                                <div class="col-12 ">
                                    <button type="submit" class="btn long mt-5"> @lang('Save') </button>
                                </div>
                            </div>
                            <!-- End Form Row -->
                        </form>
                        <!-- End Form -->
                    </div>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
@endsection
