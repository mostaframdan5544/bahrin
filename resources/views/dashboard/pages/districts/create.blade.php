@extends(config('app.dashboardFolder').'layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="card">

            <div class="card-body">

                <div class="col-lg-12">
                    <!-- Base Horizontal Form -->
                    <div class="form-element py-30 mb-30">
                        <h4 class="font-20 mb-4">@lang('create')</h4>

                        <!-- Form -->
                        <form method="POST" action="{{route($module->routeNamePrefix.'store')}}">
                            @csrf

                            @foreach(languages() as $lang)
                                <div class="billing-fields mt-5 mt-lg-0 border border-5 border-primary p-5">
                                    <h4 class="mb-4 font-20">{{__($lang->name)}}</h4>
                                    @foreach($module->translations as $translation)
                                        <div class="form-row mb-20">
                                            <label class="font-14 bold">{{__($translation)}}</label>
                                            <input type="text" value="{{old($translation.'_'.$lang->name)}} " class="theme-input-style @error($translation.'_'.$lang->name) is-invalid @enderror"  name="{{$translation.'_'.$lang->name}}"placeholder="{{__('enter here')}}">
                                            @error($translation.'_'.$lang->name)
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                            @enderror
                                            
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach

                            <!-- Form Row -->
                            <div class="form-row ">
                                <div class="col-12 ">
                                    <button type="submit" class="btn long mt-5"> @lang('Save') </button>
                                </div>
                            </div>
                            <!-- End Form Row -->
                        </form>
                        <!-- End Form -->
                    </div>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
@endsection
