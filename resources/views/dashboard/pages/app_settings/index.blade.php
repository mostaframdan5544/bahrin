@extends(config('app.dashboardFolder').'layouts.app')

@section('content')

    <div class="widget-content widget-content-area br-6">
        <div class="row">
            <div class="col-12">
                <!-- Card -->
                <div class="card bg-transparent">
                    @include(config('app.dashboardFolder').'inc.session')
                    <!-- /.session-messages -->
                    <div class="datatable" data-route="{{route($module->routeNamePrefix.'index')}}" >
                        {!!  $module->main() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
