@foreach($records as $record)
    <tr>
        @foreach($columns as $column)
            <td>{{$record->{$column} }}</td>
        @endforeach
        <td>
            @if($record->updateByAuth)
                <a href="{{route('admins.show',$record->updateByAuth->id)}}" target="_blank" rel="noopener noreferrer">{{$record->updateByAuth->name}}</a>
            @endif
        </td>
        <td >
            @include(config('app.dashboardFolder').'buttons.actions',['routeNamePrefix'=>$routeNamePrefix,'type'=>'edit-show'])
        </td>
    </tr>
@endforeach
