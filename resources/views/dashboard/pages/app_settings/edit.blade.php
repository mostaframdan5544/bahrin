@extends(config('app.dashboardFolder').'layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="card">

            <div class="card-body">

                <div class="col-lg-12">
                    <!-- Base Horizontal Form -->
                    <div class="form-element py-30 mb-30">
                        <h4 class="font-20 mb-4">@lang('edit')</h4>

                        <!-- Form -->
                        <form method="POST" action="{{route($module->routeNamePrefix.'update',$record->id)}}" enctype="multipart/form-data" >
                            @csrf
                            @method("PUT")
                            <input type="hidden" name="id" value="{{$record->id}}">
                            

                            @foreach(languages() as $lang)
                                <div class="billing-fields mt-5 mt-lg-0 border border-5 border-primary p-5">
                                        @foreach($module->translations as $translation)
                                        @if($loop->first)
                                            <h4 class="mb-4 font-20">{{__($lang->name)}} : {{__($translation)}}</h4>
                                        @endif
                                        <div class="form-row mb-20">
                                            @include(config('app.dashboardFolder').'inc.rich-text',[
                                                'name'=>$translation.'_'.$lang->name,
                                                'value'=>$record->getContent($lang,$translation)
                                            ])
                                            @error($translation.'_'.$lang->name)
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                            @enderror
                                            
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            

                            <div class="form-row mb-20 mt-4">
                                <label class="font-14 bold">{{__('phone')}}</label>
                                <textarea class="theme-input-style @error('phone') is-invalid @enderror"  name="phone"placeholder="{{__('enter here')}}">{{$record->phone}}</textarea>
                                @error('phone')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <hr>
                             
                            <div class="form-row mb-20 mt-4">
                                <label class="font-14 bold">{{__('email')}}</label>
                                <textarea class="theme-input-style @error('email') is-invalid @enderror"  name="email"placeholder="{{__('enter here')}}">{{$record->email}}</textarea>
                                @error('email')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <hr>

                           
                            <!-- Form Row -->
                            <div class="form-row ">
                                <div class="col-12 ">
                                    <button type="submit" class="btn long mt-5"> @lang('Save') </button>
                                </div>
                            </div>
                            <!-- End Form Row -->
                        </form>
                        <!-- End Form -->
                    </div>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
@endsection
