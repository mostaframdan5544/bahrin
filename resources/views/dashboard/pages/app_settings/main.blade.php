<div class="row ">
    <div class="col-md-12 filtered-list-search">
        <!-- Card -->
        <!-- <form > -->
        <form method="GET" class="search-form flex-grow filters-form">

          
            <!-- End Card -->
            <!-- Contact Header -->
            <div class="contact-header d-flex align-items-sm-center media flex-column flex-sm-row bg-white mb-30">
                
            <div class="contact-header-left media-body d-flex align-items-center mr-4">
                <!-- Search Form -->
                <form action="#" class="search-form flex-grow">
                    <div class="theme-input-group style--two">
                        <input readonly  class="theme-input-style searchAjax"
                            type="search" autofocus data-ajaxfunction='getRecords'
                            placeholder="@lang('search')" value="{{request()->keyword}}"
                            name='keyword'>

                            <button type="submit"><img src="{{asset('dashboard/assets/img/svg/search-icon.svg')}}" alt=""
                            class="svg"></button>
                    </div>
                </form>
                <!-- End Search Form -->
            </div>    

            </div>
            
        </form>

        <!-- </form>  -->
        </div>
        <!-- End Contact Header -->
</div>

<div class="table-responsive">
    
    <table class="contact-list-table text-nowrap bg-white">
        <thead>
            <tr>
               
                @foreach($columns as $th)
                    <th>{{__("{$th}")}}</th>
                @endforeach
                
             
                <th>{{__('updated by')}}</th>

                <th >@lang('actions')</th>
            </tr>
        </thead>
        <tbody>
            {!! $module->tableInfo() !!}
        </tbody>
    </table>
</div>
