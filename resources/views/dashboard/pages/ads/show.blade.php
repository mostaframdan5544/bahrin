@extends(config('app.dashboardFolder').'layouts.app')


@section('content')

    <div class="content-wrapper">
        <div class="card-body">
            <table class="table table-striped gy-7 gs-7">
                <thead>

                </thead>
                <tbody>
                <tr>
                        <td class="text-capitalize font-weight-bold">@lang('start_at')</td>
                        <td>
                        {{$record->start_at}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td class="text-capitalize font-weight-bold">@lang('end_at')</td>
                        <td>
                        {{$record->end_at}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                    @if($record->link)
                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('link')</td>
                            <td>
                            <a href="{{$record->link}}" target="_blank" >{{$record->link}}</a>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    
                    @endif
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td class="text-capitalize font-weight-bold">@lang('image')</td>
                        <td>
                            <a href="{{$record->image->filename}}" target="_blank" rel="noopener noreferrer">
                                <img src="{{asset($record->image->filename)}}" alt="" srcset="">
                            </a>
                        </td>
                    </tr>
                    @if($record->store)
                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('storeName')</td>
                            <td>
                                <a href="{{route('stores.show',$record->store_id)}}" target="_blank" >{{$record->store->content}}</a></td>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    
                    @endif
                    @if($record->createByAuth)
                    <tr>
                        
                    <td class="text-capitalize font-weight-bold">@lang('created by')</td>
                        
                        <td>
                            <a href="{{route('admins.show',$record->createByAuth->id)}}" target="_blank" rel="noopener noreferrer">{{$record->createByAuth->name}}</a>
                        </td>
                    </tr>
                    @endif
                    @if($record->updated_at)

                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('updated at')</td>
                            <td>
                                {{$record->updated_at}}
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif

                    @if($record->updateByAuth)
                    <tr>
                        
                    <td class="text-capitalize font-weight-bold">@lang('updated by')</td>
                        
                        <td>
                            <a href="{{route('admins.show',$record->updateByAuth->id)}}" target="_blank" rel="noopener noreferrer">{{$record->createByAuth->name}}</a>
                        </td>
                    </tr>
                    @endif

                    
                </tbody>
            </table>
        </div>
    </div>
@endsection
