@extends(config('app.dashboardFolder').'layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="card">

            <div class="card-body">

                <div class="col-lg-12">
                    <!-- Base Horizontal Form -->
                    <div class="form-element py-30 mb-30">
                        <h4 class="font-20 mb-4">@lang('edit')</h4>

                        <!-- Form -->
                        <form method="POST" action="{{route($module->routeNamePrefix.'update',$record->id)}}" enctype="multipart/form-data" >
                            @csrf
                            @method("PUT")
                            <div class="form-group mb-4">
                                <label class="mb-2 font-14 black bold">{{__('url')}}</label>
                                <div class="input-group addon">
                                    <input value="{{$record->link}}" type="link" class="@error('link') is-invalid @enderror form-control style--two" name="link" placeholder="{{__('url')}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text black"><span class="bold mr-1"></span> www.example.com</span>
                                    </div>
                                    @error('link')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class=" form-row mb-20">
                                <label for="store" class="mb-2 black bold">{{__('store')}}</label>
                                <select class="theme-input-style @error('store_id') is-invalid @enderror" name="store_id" id="exampleSelect6">
                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}" @selected($record->store_id== $store->id)>{{$store->content}}</option>
                                    @endforeach
                                </select>
                                @error('store_id')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>

                            <div class=" form-row mb-20">
                                <label for="start_at" class="mb-2 black bold">{{__('start_at')}}</label>
                                <input type="datetime-local" value="{{$record->start_at}}" class="theme-input-style @error('start_at') is-invalid @enderror" name="start_at" id="start_at">
                                @error('start_at')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>

                            <div class=" form-row mb-20">
                                <label for="end_at" class="mb-2 black bold">{{__('end_at')}}</label>
                                <input type="datetime-local" value="{{$record->end_at}}" class="theme-input-style @error('end_at') is-invalid @enderror" name="end_at" id="end_at">
                                @error('end_at')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <br>
                            <br>
                            
                            <div class=" form-row mb-20 mt-20">

                                @include(config('app.dashboardFolder').'inc.upload-image',[
                                    'name'=>'image',
                                    'current_image_path'=>$record->image->filename   
                                ])
                            </div>
                            <br>
                            <hr>
                            <!-- Form Row -->
                            <div class="form-row ">
                                <div class="col-12 ">
                                    <button type="submit" class="btn long mt-5"> @lang('Save') </button>
                                </div>
                            </div>
                            <!-- End Form Row -->
                        </form>
                        <!-- End Form -->
                    </div>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
@endsection
