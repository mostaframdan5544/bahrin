@extends(config('app.dashboardFolder').'layouts.app')


@section('content')

    <div class="content-wrapper">
        <div class="card-body">
            <table class="table table-striped gy-7 gs-7">
                <thead>

                </thead>
                <tbody>
                    
                <tr>
                        <td class="text-capitalize font-weight-bold">@lang('name')</td>
                        <td>
                            {{$record->name}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td class="text-capitalize font-weight-bold">@lang('phone')</td>
                        <td>
                            {{$record->phone}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr>
                        <td class="text-capitalize font-weight-bold">@lang('message')</td>
                        <td>
                            {{$record->message}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    

                    <tr>
                        <td class="text-capitalize font-weight-bold">@lang('created at')</td>
                        <td>
                            {{$record->created_at}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                   
                </tbody>
            </table>
        </div>
    </div>
@endsection
