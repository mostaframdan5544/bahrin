@extends(config('app.dashboardFolder').'layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="card">

            <div class="card-body">

                <div class="col-lg-12">
                    <!-- Base Horizontal Form -->
                    <div class="form-element py-30 mb-30">
                        <h4 class="font-20 mb-4">@lang('edit')</h4>

                        <!-- Form -->
                        <form method="POST" action="{{route($module->routeNamePrefix.'update',$record->id)}}" enctype="multipart/form-data" >
                            @csrf
                            @method("PUT")
                            <input type="hidden" name="id" value="{{$record->id}}">
                            <div class=" form-row mb-20">
                                <label for="store" class="mb-2 black bold">{{__('sub store')}}</label>
                                <select class="theme-input-style @error('store_id') is-invalid @enderror" name="store_id" id="exampleSelect6">
                                    @foreach($stores as $store)
                                        <option value="{{$store->id}}" @selected($record->store_id == $store->id)>{{$store->content}}</option>
                                    @endforeach
                                </select>
                                @error('store_id')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>

                            @foreach(languages() as $lang)
                                <div class="billing-fields mt-5 mt-lg-0 border border-5 border-primary p-5">
                                    <h4 class="mb-4 font-20">{{__($lang->name)}}</h4>
                                        @foreach($module->translations as $translation)
                                        <div class="form-row mb-20">
                                            <label class="font-14 bold">{{__($translation)}}</label>
                                            <input type="text" value="{{$record->getContent($lang,$translation)}} " class="theme-input-style @error($translation.'_'.$lang->name) is-invalid @enderror"  name="{{$translation.'_'.$lang->name}}"placeholder="{{__('enter here')}}">
                                            @error($translation.'_'.$lang->name)
                                                <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                            @enderror
                                            
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                            

                            <div class="form-row mb-20 mt-4">
                                <label class="font-14 bold">{{__('address')}}</label>
                                <input value="{{$record->address}} " class="theme-input-style @error('address') is-invalid @enderror"  name="address"placeholder="{{__('enter here')}}">
                                @error('address')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <hr>
                            <div class="form-row mb-20 mt-4">
                                <label class="font-14 bold">{{__('phone')}}</label>
                                <input value="{{$record->phone}} " class="theme-input-style @error('phone') is-invalid @enderror"  name="phone"placeholder="{{__('enter here')}}">
                                @error('phone')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <hr>
                            
                            <div class="form-group mb-4">
                                <label class="mb-2 font-14 black bold">{{__('whatsappLink')}}</label>
                                <div class="input-group addon">
                                    <input value="{{$record->whatsappLink}}" type="whatsappLink" class="@error('whatsappLink') is-invalid @enderror form-control style--two" name="whatsappLink" placeholder="{{__('url')}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text black"><span class="bold mr-1"></span> https://wa.me/+97312345678</span>
                                    </div>
                                    @error('whatsappLink')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group mb-4">
                                <label class="mb-2 font-14 black bold">{{__('instaLink')}}</label>
                                <div class="input-group addon">
                                    <input value="{{$record->instaLink}}" type="instaLink" class="@error('instaLink') is-invalid @enderror form-control style--two" name="instaLink" placeholder="{{__('url')}}">
                                    <div class="input-group-append">
                                        <span class="input-group-text black"><span class="bold mr-1"></span> https://www.instagram.com/moebahrain</span>
                                    </div>
                                    @error('instaLink')
                                        <div class="invalid-feedback">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <hr>
                            
                             
                            <div class="form-row mb-20 mt-4">
                                <label class="font-14 bold">{{__('image')}}</label>
                                <input type="file" accept="image/*"  value="{{old('image')}} " class="theme-input-style @error('image') is-invalid @enderror"  name="image"placeholder="{{__('enter here')}}">
                                @error('image')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <hr>


                           
                            <!-- Form Row -->
                            <div class="form-row ">
                                <div class="col-12 ">
                                    <button type="submit" class="btn long mt-5"> @lang('Save') </button>
                                </div>
                            </div>
                            <!-- End Form Row -->
                        </form>
                        <!-- End Form -->
                    </div>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
@endsection
