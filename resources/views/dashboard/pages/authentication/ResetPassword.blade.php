@extends(config('app.dashboardFolder').'layouts.app')

@section('content')

    <div class="widget-content widget-content-area br-6">
        <div class="row">
            <div class="col-12">
                <!-- Card -->
                <div class="card bg-transparent">
                    @include(config('app.dashboardFolder').'inc.session')
                    <!-- /.session-messages -->
                    <div class="datatable"  >
                        <div class="container-fluid">
                            <div class="card">

                                <div class="card-body">

                                    <div class="col-lg-12">
                                        <!-- Base Horizontal Form -->
                                        <div class="form-element py-30 mb-30">
                                            <h4 class="font-20 mb-4">@lang('resetPassword')</h4>

                                            <!-- Form -->
                                            <form method="POST" action="{{route('resetPassword')}}" enctype="multipart/form-data" >
                                                @csrf
                                                @method("PUT")
                                                
                                                <div class="form-row mb-20 mt-4">
                                                    <label class="font-14 bold">{{__('old password')}}</label>
                                                    <input value="{{old('old password')}} " class="theme-input-style @error('old password') is-invalid @enderror"  name="old_password"placeholder="{{__('enter here')}}">
                                                    @error('old password')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                    @enderror
                                                </div>
                                                    
                                                <div class="form-group mb-4">
                                                    <label for="password" class="mb-2 black bold">{{__('password')}}</label>
                                                    <input type="password" class="theme-input-style @error('password') is-invalid @enderror" id="password" name="password" placeholder="{{__('enter here')}}">
                                                    @error('password')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                    @enderror
                                                </div>
                                                <div class="form-group mb-4">
                                                    <label for="password_confirmation" class="mb-2 black bold">{{__('password_confirmation')}}</label>
                                                    <input type="password" class="theme-input-style @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="{{__('enter here')}}">
                                                    @error('password_confirmation')
                                                        <div class="invalid-feedback">
                                                            {{$message}}
                                                        </div>
                                                    @enderror
                                                </div>
                                                <!-- Form Row -->
                                                <div class="form-row ">
                                                    <div class="col-12 ">
                                                        <button type="submit" class="btn long mt-5"> @lang('Save') </button>
                                                    </div>
                                                </div>


                                                <!-- End Form Row -->
                                            </form>
                                            <!-- End Form -->
                                        </div>
                                        <!-- End Horizontal Form -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
