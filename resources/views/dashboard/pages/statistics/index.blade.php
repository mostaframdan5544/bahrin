@extends('dashboard.layouts.app')

@section('content')
    @push('styles')
        <link href="{{asset(Config::get('app.locale').'/plugins/apex/apexcharts.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset(Config::get('app.locale').'/assets/css/dashboard/dash_2.css')}}" rel="stylesheet" type="text/css" />
    @endpush
      <!-- Main Content -->
             
         <div class="row">
            <div class="col-xl-6 col-lg-12">
               <div class="row">
                  <div class="col-12">
                     <!-- Card -->
                     <div class="card mb-30">
                        <div class="card-body d-flex justify-content-between mb-n72">
                           <div class="position-relative index-9">
                              <h4 class="mb-1">Website Analytics</h4>
                              <p class="font-14">Check out each column for more details</p>
                           </div>

                           <!-- Dropdown Button -->
                           <div class="dropdown-button">
                              <a href="#" class="d-flex align-items-center" data-toggle="dropdown">
                                 <div class="menu-icon style--two mr-0">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                 </div>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right">
                                 <a href="#">Daily</a>
                                 <a href="#">Sort By</a>
                                 <a href="#">Monthly</a>
                              </div>
                           </div>
                           <!-- End Dropdown Button -->

                        </div>
                        <div id="apex_column-chart"></div>
                     </div>
                     <!-- End Card -->
                  </div>
                  <div class="col-md-4 col-sm-6">
                     <!-- Card -->
                     <div class="card mb-30 progress_1">
                        <div class="card-body">
                           <h4 class="progress-title">{{__('users')}}</h4>

                           <div class="ProgressBar-wrap position-relative mb-4">
                              <div class="ProgressBar ProgressBar_1" data-progress="75">
                                 <svg class="ProgressBar-contentCircle" viewBox="0 0 200 200">
                                    <!-- on défini le l'angle et le centre de rotation du cercle -->
                                    <circle transform="rotate(135, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="8" />
                                    <circle transform="rotate(135, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="85" />
                                 </svg>
                                 <span class="ProgressBar-percentage ProgressBar-percentage--count"></span>
                              </div>
                           </div>

                           <div class="d-flex flex-wrap mb-2 progress-info">
                              <div>{{__('total')}}</div>
                              <div><img src="assets/img/svg/caret-up.svg" alt="" class="svg">{{$total_users}}</div>
                           </div>

                        </div>
                     </div>
                     <!-- End Card -->
                  </div>

                  <div class="col-md-4 col-sm-6">
                     <!-- Card -->
                     <div class="card mb-30 progress_2">
                        <div class="card-body">
                           <h4 class="progress-title">{{__('Stores')}} </h4>

                           <div class="ProgressBar-wrap position-relative mb-4">
                              <div class="ProgressBar ProgressBar_2" data-progress="35">
                                 <svg class="ProgressBar-contentCircle" viewBox="0 0 200 200">
                                    <!-- on défini le l'angle et le centre de rotation du cercle -->
                                    <circle transform="rotate(135, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="85" />
                                    <circle transform="rotate(135, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="85" />
                                 </svg>
                                 <span class="ProgressBar-percentage--text">{{__('total')}}</span>
                                 <span class="ProgressBar-percentage ProgressBar-percentage--count"></span>
                              </div>
                           </div>
                           
                           <div class="d-flex flex-wrap progress-info">
                              <div>{{__('total')}}</div>
                              <div><img src="assets/img/svg/caret-up.svg" alt="" class="svg">{{$total_stores}}</div>
                           </div>
                        </div>
                     </div>
                     <!-- End Card -->
                  </div>

                  <div class="col-md-4 col-sm-6">
                     <!-- Card -->
                     <div class="card mb-30 progress_3 mr-0">
                        <div class="card-body">
                           <h4 class="progress-title">{{__('categories')}}</h4>

                           <div class="ProgressBar-wrap position-relative mb-4">
                              <div class="ProgressBar ProgressBar_3" data-progress="70">
                                 <svg class="ProgressBar-contentCircle" viewBox="0 0 200 200">
                                    <!-- on défini le l'angle et le centre de rotation du cercle -->
                                    <circle transform="rotate(135, 100, 100)" class="ProgressBar-background" cx="100" cy="100" r="85" stroke-width="20" />
                                    <circle transform="rotate(135, 100, 100)" class="ProgressBar-circle" cx="100" cy="100" r="85" stroke-width="20" />
                                 </svg>
                                 <span class="ProgressBar-percentage--text"> Increase </span>
                                 <span class="ProgressBar-percentage ProgressBar-percentage--count"></span>
                              </div>
                           </div>

                           <div class="d-flex flex-wrap progress-info">
                              <div>{{__('total')}}</div>
                              <div><img src="assets/img/svg/caret-up.svg" alt="" class="svg">{{$total_categories}}</div>
                           </div>
                        </div>
                     </div>
                     <!-- End Card -->
                  </div>
               </div>
            </div>
            <div class="col-xl-6 col-lg-12">
               <div class="row">
                  <div class="col-sm-6">
                     <!-- Card -->
                     <div class="card mb-30">
                        <div class="card-body">
                           <div id="apex_line-chart"></div>

                           <div class="d-flex align-items-end justify-content-between mt-4">
                              <div class="">
                                 <h4 class="mb-1">Website Analytics</h4>
                                 <p class="font-14">Check out each column for more details</p>
                              </div>

                              <div class="dropdown-button">
                                 <a href="#" class="d-flex align-items-center" data-toggle="dropdown">
                                    <div class="menu-icon justify-content-end pb-1 style--two mr-0">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                    </div>
                                 </a>
                                 <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#">Daily</a>
                                    <a href="#">Sort By</a>
                                    <a href="#">Monthly</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- End Card -->
                  </div>

                  <div class="col-sm-6">
                     <!-- Card -->
                     <div class="card mb-30">
                        <div class="card-body">
                           <div id="apex_line2-chart"></div>

                           <div class="d-flex align-items-end justify-content-between mt-4">
                              <div class="">
                                 <h4 class="mb-1">Company Growth</h4>
                                 <p class="font-14">Company is growing 20% in average</p>
                              </div>

                              <div class="dropdown-button">
                                 <a href="#" class="d-flex align-items-center" data-toggle="dropdown">
                                    <div class="menu-icon justify-content-end pb-1 style--two mr-0">
                                       <span></span>
                                       <span></span>
                                       <span></span>
                                    </div>
                                 </a>
                                 <div class="dropdown-menu dropdown-menu-right">
                                    <a href="#">Daily</a>
                                    <a href="#">Sort By</a>
                                    <a href="#">Monthly</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- End Card -->
                  </div>

                  <div class="col-12">
                     <!-- Card -->
                     <div class="card todo-list mb-30">
                        <div class="card-body p-0">
                           <!-- Todo Single -->
                           <div class="single-row border-bottom pt-3 pb-2">
                              <div class="d-flex justify-content-between align-items-start mb-2">
                                 <div class="">
                                    <h4 class="card-title">Today To Do List</h4>
                                    <p class="card-text font-14 bold">Saturday, <br />
                                       12 October 2019</p>
                                 </div>

                                 <div class="d-flex align-items-center">
                                    <a href="pages/apps/todolist/add-new.html" class="btn-circle">
                                       <img src="assets/img/svg/plus_white.svg" alt="" class="svg">
                                    </a>

                                    <div class="dropdown-button">
                                       <a href="#" class="d-flex align-items-center" data-toggle="dropdown">
                                          <div class="menu-icon style--two justify-content-center mr-0">
                                             <span></span>
                                             <span></span>
                                             <span></span>
                                          </div>
                                       </a>
                                       <div class="dropdown-menu dropdown-menu-right">
                                          <a href="#">Daily</a>
                                          <a href="#">Sort By</a>
                                          <a href="#">Monthly</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- End Todo Single -->
                           
                           <!-- Todo Single -->
                           <div class="single-row border-bottom pt-3 pb-3">
                              <div class="d-flex justify-content-between align-items-center">
                                 <div class="d-flex position-relative">
                                    <!-- Custom Checkbox -->
                                    <label class="custom-checkbox">
                                       <input type="checkbox" checked>
                                       <span class="checkmark"></span>
                                    </label>
                                    <!-- End Custom Checkbox -->

                                    <!-- Todo Text -->
                                    <div class="todo-text line-through">
                                       <p class="card-text mb-1">For detracty charmed add talking age. Shy resolution instrument unreserved man few.</p>
                                       <p class="text-warning font-12 mb-0">Urgent Not Important</p>
                                    </div>
                                    <!-- End Todo Text -->
                                 </div>
   
                                 <div class="d-flex">
                                    <!-- Assign To -->
                                    <div class="assign_to">
                                       <img src="assets/img/svg/Info.svg" alt="" class="svg mr-2 mb-1">
                                       <div class="assign-content">
                                          <div class="font-12 text-warning">Back-End</div>
                                          <img src="assets/img/avatar/info-avatar.png" alt="" class="assign-avatar">
                                       </div>
                                    </div>
                                    <!-- End Assign To -->

                                    <!-- Drag Icon -->
                                    <img src="assets/img/svg/dragicon.svg" alt="" class="svg">
                                    <!-- End Drag Icon -->
   
                                    <!-- Dropdown Button -->
                                    <div class="dropdown-button">
                                       <a href="#" class="d-flex align-items-center" data-toggle="dropdown">
                                          <div class="menu-icon style--two w-14 mr-0">
                                             <span></span>
                                             <span></span>
                                             <span></span>
                                          </div>
                                       </a>
                                       <div class="dropdown-menu dropdown-menu-right">
                                          <a href="#">Daily</a>
                                          <a href="#">Sort By</a>
                                          <a href="#">Monthly</a>
                                       </div>
                                    </div>
                                    <!-- End Dropdown Button -->
                                 </div>
                              </div>
                           </div>
                           <!-- End Todo Single -->

                        </div>
                     </div>
                     <!-- End Card -->
                  </div>
               </div>
            </div>
         </div>
         
         <!-- End Main Content -->
    @push('scripts')
        <script src="{{asset(Config::get('app.locale').'/plugins/apex/apexcharts.min.js')}}"></script>
        <script src="{{asset(Config::get('app.locale').'/assets/js/dashboard/dash_2.js ')}}"></script>
    @endpush
@endsection