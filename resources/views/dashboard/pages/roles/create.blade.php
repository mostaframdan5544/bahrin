@extends(config('app.dashboardFolder').'layouts.app')


@section('content')
    <div class="container-fluid">
        <div class="card">

            <div class="card-body">

                <div class="col-lg-12">
                    <!-- Base Horizontal Form -->
                    <div class="form-element py-30 mb-30">
                        <h4 class="font-20 mb-4">@lang('create')</h4>

                        <!-- Form -->
                        <form method="POST" action="{{route($module->routeNamePrefix.'store')}}">
                            @csrf

                            <div class="form-row mb-20">
                                <label class="font-14 bold">{{__('name')}}</label>
                                <input type="text" value="{{old('name')}} " class="theme-input-style @error('name') is-invalid @enderror"  name="name"placeholder="{{__('enter here')}}">
                                @error('name')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                @enderror
                            </div>
                            <hr>
                            <div class="row">
                                @foreach(config('permissions.modules') as $module)
                                    
                                    @if($loop->first )
                                        <div class="col-sm-12 text-center m-5">
                                            <h2 class="mb-4 font-40 m-3">{{__('permissions')}}</h2>
                                        </div>
                                        <hr>

                                        <div class="col-sm-12  m-5">
                                            <div class="form-group mb-5 text-center">
                                                <label for="" class="text-capitalize font-weight-bold text-info text-center">@lang('all')</label>
                                                <input type="checkbox" name="all"  value="{{old('name')}} " class="form-control form-control-solid">
                                            </div>
                                        </div>
                                        
                                    @endif

                                    <div class="col-md-4">
                                        
                                        <div class="form-group mb-5">
                                            <h4 class='text-center mb-2'>{{__("{$module['name']}")}}</h4>
                                            <ul class="list-group task-list-group">
                                                @foreach(permissionInfo($module) as $action)
                                                    <li class="list-group-item list-group-item-action ">
                                                        <div class="n-chk">
                                                            <label class="new-control new-checkbox checkbox-primary w-100 justify-content-between">
                                                            <input type="checkbox" class="new-control-input" @checked(old($action['input'])) name="{{$action['input']}}">
                                                            <span class="new-control-indicator"></span>
                                                                <span class="ml-3 d-block">
                                                                    <span class="badge badge-{{$action['bg']}}">{{$action['display']}}</span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul> 
                                        </div>
                                    </div>
                                @endforeach
                        </div>
                            <!-- Form Row -->
                            <div class="form-row ">
                                <div class="col-12 ">
                                    <button type="submit" class="btn long mt-5"> @lang('Save') </button>
                                </div>
                            </div>
                            <!-- End Form Row -->
                        </form>
                        <!-- End Form -->
                    </div>
                    <!-- End Horizontal Form -->
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $("body").on("input","input[name='all']",function(){
                $("input[type='checkbox']").attr('checked',$(this).is(':checked'))
            });
        </script>
    @endpush
@endsection
