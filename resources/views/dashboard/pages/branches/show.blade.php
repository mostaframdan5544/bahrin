@extends(config('app.dashboardFolder').'layouts.app')


@section('content')

    <div class="content-wrapper">
        <div class="card-body">
            <table class="table table-striped gy-7 gs-7">
                <thead>

                </thead>
                <tbody>
                    @foreach(languages() as $lang)
                        <tr>
                            <td>
                                <div class="billing-fields mt-5 mt-lg-0 border border-5 border-primary p-5">
                                    <h4 class="mb-4 font-20 text-danger">{{__($lang->name)}}</h4>
                                    @foreach(config('app.stores_translations') as $translation)
                                        <div class="form-row ">
                                            <h4 class="text-capitalize font-weight-bold">{{__($translation)}} : </h4>
                                            <span class="text-capitalize font-weight-bold">{{$record->getContent($lang,$translation)}}</span>
                                        </div>
                                    @endforeach
                                </div>
                            </td>
                        @endforeach
                    </tr>
                   
                    @if($record->image)
                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('image')</td>
                            <td>
                                <a href="{{$record->image->filename}}" target="_blank" rel="noopener noreferrer">
                                    <img src="{{asset($record->image->filename)}}" alt="" srcset="">
                                </a>
                            </td>
                        </tr>
                    @endif
                    
                    @if($record->store)
                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('storeName')</td>
                            <td>
                                <a href="{{route('stores.show',$record->store_id)}}" target="_blank" >{{$record->storeName}}</a></td>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    
                    @endif

                    <tr>
                        <td class="text-capitalize font-weight-bold">@lang('created at')</td>
                        <td>
                            {{$record->created_at}}
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                    @if($record->instaLink)
                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('instaLink')</td>
                            <td>
                                <a href="{{$record->instaLink}}" target="_blank" rel="noopener noreferrer">{{$record->instaLink}}</a>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif

                    @if($record->whatsappLink)
                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('whatsappLink')</td>
                            <td>
                                <a href="{{$record->whatsappLink}}" target="_blank" rel="noopener noreferrer">{{$record->whatsappLink}}</a>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif
                   
                    @if($record->createByAuth)
                    <tr>
                        
                        <td class="text-capitalize font-weight-bold">@lang('created by')</td>
                        
                        <td>
                            <a href="{{route('admins.show',$record->createByAuth->id)}}" target="_blank" rel="noopener noreferrer">{{$record->createByAuth->name}}</a>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                    @if($record->updated_at)

                        <tr>
                            <td class="text-capitalize font-weight-bold">@lang('updated at')</td>
                            <td>
                                {{$record->updated_at}}
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                    @endif

                    @if($record->updateByAuth)
                    <tr>
                        
                    <td class="text-capitalize font-weight-bold">@lang('updated by')</td>
                        
                        <td>
                            <a href="{{route('admins.show',$record->updateByAuth->id)}}" target="_blank" rel="noopener noreferrer">{{$record->createByAuth->name}}</a>
                        </td>
                    </tr>
                    @endif

                    
                </tbody>
            </table>
        </div>
    </div>
@endsection
